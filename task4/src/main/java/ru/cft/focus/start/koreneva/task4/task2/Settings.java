package ru.cft.focus.start.koreneva.task4.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

class Settings implements ISettings {
    private final Properties property;
    private final Logger logger;

    private int costumerCount;
    private int manufacturersCount;
    private int costumerSleepTime;
    private int manufacturersSleepTime;
    private int warehouseSize;

    Settings() throws IOException {
        property = new Properties();
        logger = LoggerFactory.getLogger(getClass());

        loadProperty();
    }

    @Override
    public int getCostumerCount() {
        return costumerCount;
    }

    @Override
    public int getManufacturersCount() {
        return manufacturersCount;
    }

    @Override
    public int getCostumerSleepTime() {
        return costumerSleepTime;
    }


    @Override
    public int getManufacturersSleepTime() {
        return manufacturersSleepTime;
    }

    @Override
    public int getWarehouseSize() {
        return warehouseSize;
    }

    private void loadProperty() throws IOException {
        String filePath = getClass().getResource("/config.properties").getFile();
        try (FileInputStream file = new FileInputStream(filePath)) {
            property.load(file);

            costumerCount = getInt("costumerCount");
            manufacturersCount = getInt("manufacturersCount");
            costumerSleepTime = getInt("costumerSleepTime");
            manufacturersSleepTime = getInt("manufacturersSleepTime");
            warehouseSize = getInt("warehouseSize");
        } catch (IOException e) {
            logger.error("Property file not found.", e);
            throw e;
        }
    }

    private int getInt(String name) {
        return Integer.parseInt(property.getProperty(name));
    }
}
