package ru.cft.focus.start.koreneva.task4.task2;

interface ISettings {
    int getCostumerCount();

    int getManufacturersCount();

    int getCostumerSleepTime();

    int getManufacturersSleepTime();

    int getWarehouseSize();
}
