package ru.cft.focus.start.koreneva.task4.task1;

import java.math.BigDecimal;

public interface ICalculationService {
    BigDecimal calculate(int i);
}
