package ru.cft.focus.start.koreneva.task4.task2;

class Resource {
    private static int idCounter = 0;
    private final int id;

    Resource() {
        this.id = idCounter++;
    }

    int getId() {
        return id;
    }
}
