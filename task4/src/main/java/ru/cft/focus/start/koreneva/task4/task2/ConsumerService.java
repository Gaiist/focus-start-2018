package ru.cft.focus.start.koreneva.task4.task2;

import java.util.ArrayList;
import java.util.List;

class ConsumerService {
    private final List <Consumer> consumers;
    private final ThreadLocal <List <Thread>> threads;

    ConsumerService(Warehouse warehouse, int costumerCount, int sleepTime) {
        consumers = new ArrayList <>();
        threads = ThreadLocal.withInitial(ArrayList::new);

        for (int id = 0; id < costumerCount; id++) {
            consumers.add(new Consumer(id, warehouse, sleepTime));
        }
    }

    void start() {
        for (Consumer consumer : consumers) {
            Thread thread = new Thread(consumer);
            threads.get().add(thread);
            thread.start();
        }
    }
}
