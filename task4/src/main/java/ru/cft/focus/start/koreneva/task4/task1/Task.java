package ru.cft.focus.start.koreneva.task4.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

public class Task implements Callable <BigDecimal> {
    private final ICalculationService calculationService;
    private final int startIndex;
    private final int endIndex;
    private final Logger logger;

    Task(ICalculationService calculationService, int startIndex, int endIndex) {
        this.calculationService = calculationService;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        logger = LoggerFactory.getLogger(getClass());
    }

    @Override
    public BigDecimal call() {
        BigDecimal result = BigDecimal.ZERO;

        for (int i = startIndex; i < endIndex; i++) {
            result = result.add(calculationService.calculate(i));
        }

        logger.info("Calculation {}", result);
        return result;
    }
}
