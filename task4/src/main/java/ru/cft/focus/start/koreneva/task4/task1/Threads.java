package ru.cft.focus.start.koreneva.task4.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

class Threads {
    private static final int END_INDEX = 1000000;
    private static final int INCREASE_INDEX = 1000;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    void runCalculations() {
        int iterationCount = END_INDEX / INCREASE_INDEX;
        ExecutorService executorService = Executors.newFixedThreadPool(iterationCount);
        ICalculationService calculationService = new CalculationService();
        BigDecimal result = BigDecimal.ZERO;
        List <Future <BigDecimal>> futures = new ArrayList <>();

        for (int i = 1; i < END_INDEX; i += INCREASE_INDEX) {
            Task task = new Task(calculationService, i, INCREASE_INDEX + i);
            Future <BigDecimal> future = executorService.submit(task);
            futures.add(future);
        }

        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            logger.error("Interrupted await termination.", e);
            Thread.currentThread().interrupt();
        }

        for (Future <BigDecimal> future : futures) {
            try {
                result = result.add(future.get());
            } catch (InterruptedException e) {
                logger.error("Interrupted Exception");
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
                logger.error("Execution Exception");
            }
        }

        executorService.shutdown();
        logger.info("Result = {}", result);
    }
}
