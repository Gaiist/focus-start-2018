package ru.cft.focus.start.koreneva.task4.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayDeque;
import java.util.Queue;

class Warehouse {
    private final int warehouseSize;
    private final Queue <Resource> queue;
    private final Logger logger;

    Warehouse(int warehouseSize) {
        this.warehouseSize = warehouseSize;
        queue = new ArrayDeque <>();
        logger = LoggerFactory.getLogger("Warehouse");
    }

    synchronized void add(Resource resources) {
        while (queue.size() >= warehouseSize) {
            try {
                wait();
            } catch (InterruptedException e) {
                logger.error("InterruptedException");
                Thread.currentThread().interrupt();
            }
        }
        queue.add(resources);
        notifyAll();
    }

    synchronized Resource get() {
        while (queue.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                logger.error("InterruptedException");
                Thread.currentThread().interrupt();
            }
        }
        notifyAll();
        return queue.poll();
    }

    synchronized int size() {
        return queue.size();
    }
}
