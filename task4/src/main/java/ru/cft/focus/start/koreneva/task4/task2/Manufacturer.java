package ru.cft.focus.start.koreneva.task4.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

class Manufacturer implements Runnable {
    private final Warehouse warehouse;
    private final int id;
    private final int sleepTime;
    private final Logger logger;
    private boolean isRun;

    Manufacturer(int id, Warehouse warehouse, int sleepTime) {
        this.id = id;
        this.warehouse = warehouse;
        this.sleepTime = sleepTime;
        logger = LoggerFactory.getLogger("Manufacturer");
        this.isRun = false;
    }

    @Override
    public void run() {
        isRun = true;

        while (isRun) {
            try {
                TimeUnit.SECONDS.sleep(sleepTime);
                Resource resource = new Resource();
                warehouse.add(resource);
                logger.info("RecourseID={} manufactured {}", resource.getId(), warehouse.size());

            } catch (InterruptedException e) {
                logger.error("InterruptedException");
                Thread.currentThread().interrupt();
            }
        }
    }

    public int getId() {
        return id;
    }

    public void stop() {
        isRun = false;
    }
}
