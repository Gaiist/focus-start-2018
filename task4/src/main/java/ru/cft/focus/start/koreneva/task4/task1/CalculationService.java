package ru.cft.focus.start.koreneva.task4.task1;

import java.math.BigDecimal;

class CalculationService implements ICalculationService {
    @Override
    public BigDecimal calculate(int i) {
        return BigDecimal.valueOf(1d / ( i * ( 1d + i ) ));
    }
}
