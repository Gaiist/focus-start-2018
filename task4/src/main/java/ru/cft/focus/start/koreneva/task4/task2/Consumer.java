package ru.cft.focus.start.koreneva.task4.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Consumer implements Runnable {
    private final Warehouse warehouse;
    private final int id;
    private final int sleepTime;
    private final ThreadLocal <List <Resource>> resources;
    private final Logger logger;

    private boolean isRun;

    Consumer(int id, Warehouse warehouse, int sleepTime) {
        this.id = id;
        this.warehouse = warehouse;
        this.sleepTime = sleepTime;
        resources = ThreadLocal.withInitial(ArrayList::new);
        logger = LoggerFactory.getLogger("Consumer");
        isRun = false;
    }

    @Override
    public void run() {
        logger.info("Start consumer#{}", id);
        isRun = true;

        while (isRun) {
            try {
                TimeUnit.SECONDS.sleep(sleepTime);
                Resource resource = warehouse.get();
                resources.get().add(resource);
                logger.info("RecourseID={} consumed. {}", resource.getId(), warehouse.size());
            } catch (InterruptedException e) {
                logger.error("InterruptedException");
                Thread.currentThread().interrupt();
            }
        }
    }
}
