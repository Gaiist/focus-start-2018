package ru.cft.focus.start.koreneva.task4.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Solution {
    private static Logger logger = LoggerFactory.getLogger("Solution");

    public static void main(String[] args) {
        try {
            ISettings settings = new Settings();
            Warehouse warehouse = new Warehouse(settings.getWarehouseSize());

            ManufacturerService manufacturerService = new ManufacturerService(warehouse, settings.getManufacturersCount(), settings.getManufacturersSleepTime());
            ConsumerService consumersService = new ConsumerService(warehouse, settings.getCostumerCount(), settings.getCostumerSleepTime());

            manufacturerService.start();
            consumersService.start();
        } catch (IOException e) {
            logger.error("Settings file exception.", e);
        }
    }
}