package ru.cft.focus.start.koreneva.task4.task2;

import java.util.ArrayList;
import java.util.List;

class ManufacturerService {
    private final List <Manufacturer> manufacturers;
    private final ThreadLocal <List <Thread>> threads;

    ManufacturerService(Warehouse warehouse, int manufacturersCount, int sleepTime) {
        manufacturers = new ArrayList <>();
        threads = ThreadLocal.withInitial(ArrayList::new);

        for (int id = 0; id < manufacturersCount; id++) {
            manufacturers.add(new Manufacturer(id, warehouse, sleepTime));
        }
    }

    void start() {
        for (Manufacturer manufacturer : manufacturers) {
            Thread thread = new Thread(manufacturer);
            threads.get().add(thread);
            thread.start();
        }
    }
}
