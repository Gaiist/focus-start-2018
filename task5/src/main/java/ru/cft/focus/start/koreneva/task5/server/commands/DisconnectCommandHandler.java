package ru.cft.focus.start.koreneva.task5.server.commands;

import ru.cft.focus.start.koreneva.task5.common.commands.client.UserConnectedClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.common.exceptions.runtime.ErrorRuntimeException;
import ru.cft.focus.start.koreneva.task5.server.UserConnection;
import ru.cft.focus.start.koreneva.task5.server.UserConnections;
import ru.cft.focus.start.koreneva.task5.server.core.ICommandHandler;

import java.io.IOException;

public class DisconnectCommandHandler implements ICommandHandler {
    @Override
    public void handle(UserConnection userConnection, UserConnections userConnections, ServerCommand command) {
        userConnections.removeConnection(userConnection);
        UserConnectedClientCommand userConnectedClientCommand = new UserConnectedClientCommand();
        userConnectedClientCommand.setName(userConnection.getUser().getName());
        userConnectedClientCommand.setConnected(false);
        userConnection.getUser().setActive(false);
        try {
            userConnections.sendAll(userConnection, userConnectedClientCommand);
        } catch (IOException e) {
            throw new ErrorRuntimeException("User connections can't send command");
        }
    }
}
