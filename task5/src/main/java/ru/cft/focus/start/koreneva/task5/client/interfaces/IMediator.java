package ru.cft.focus.start.koreneva.task5.client.interfaces;

import ru.cft.focus.start.koreneva.task5.client.view.UserInfoDialogResult;

import java.util.List;

public interface IMediator {
    void registerComponent(IComponent component);

    UserInfoDialogResult getResultStartDialog();

    void showErrorDialog(String message);

    void createGUI();

    void setUsers(List <String> userList);

    //  void updateMessage(String message);

    void displayMessage(String message);

    void setNickname(String nickName);

    void clearInputField();

    String getMessage();

}
