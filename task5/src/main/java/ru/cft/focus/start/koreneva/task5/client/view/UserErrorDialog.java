package ru.cft.focus.start.koreneva.task5.client.view;

import javax.swing.*;

class UserErrorDialog {
    private static final String TITLE = "ERROR!";
    private final String errorMessage;

    UserErrorDialog(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    void showErrorPane() {
        JOptionPane.showMessageDialog(null, errorMessage, TITLE, JOptionPane.ERROR_MESSAGE);
    }
}
