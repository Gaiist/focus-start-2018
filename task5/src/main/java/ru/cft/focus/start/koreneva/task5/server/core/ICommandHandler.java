package ru.cft.focus.start.koreneva.task5.server.core;

import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.server.UserConnection;
import ru.cft.focus.start.koreneva.task5.server.UserConnections;

public interface ICommandHandler {
    void handle(UserConnection userConnection, UserConnections userConnections, ServerCommand command);
}
