package ru.cft.focus.start.koreneva.task5.server;

public class User {
    private String name;
    private boolean isActive;

    User() {
        isActive = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
