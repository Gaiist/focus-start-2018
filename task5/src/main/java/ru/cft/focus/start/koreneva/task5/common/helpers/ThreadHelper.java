package ru.cft.focus.start.koreneva.task5.common.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreadHelper {
    private static Logger logger = LoggerFactory.getLogger("ThreadHelper");

    private ThreadHelper() {
    }

    public static void trySleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            logger.error("Thread Helper is interrupted.");
            Thread.currentThread().interrupt();
        }
    }
}
