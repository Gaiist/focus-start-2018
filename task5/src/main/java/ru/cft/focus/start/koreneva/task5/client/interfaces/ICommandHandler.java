package ru.cft.focus.start.koreneva.task5.client.interfaces;

import ru.cft.focus.start.koreneva.task5.client.ChatService;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;

public interface ICommandHandler {
    void handle(ChatService mediator, ClientCommand command);
}
