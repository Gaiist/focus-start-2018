package ru.cft.focus.start.koreneva.task5.common.interfaces;

import ru.cft.focus.start.koreneva.task5.common.network.TCPConnection;

public interface ITCPConnectionListener {
    void onConnectionReady(TCPConnection tcpConnection);

    void onReceiveString(TCPConnection tcpConnection, String value);

    void onDisconnect(TCPConnection tcpConnection);

    void onException(TCPConnection tcpConnection, Exception e);
}
