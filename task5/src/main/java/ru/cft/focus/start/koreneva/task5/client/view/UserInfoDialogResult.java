package ru.cft.focus.start.koreneva.task5.client.view;

public class UserInfoDialogResult {
    private final String nickName;
    private final String address;

    UserInfoDialogResult(String nickName, String address) {
        this.nickName = nickName;
        this.address = address;
    }

    public String getNickName() {
        return nickName;
    }

    public String getAddress() {
        return address;
    }
}
