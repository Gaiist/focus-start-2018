package ru.cft.focus.start.koreneva.task5.common.commands.server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DisconnectServerCommand")
public class DisconnectServerCommand extends ServerCommand {
    private String message;

    public DisconnectServerCommand() {
        setCommandType(ServerCommandType.DISCONNECT);
    }

    public String getMessage() {
        return message;
    }

    @XmlElement(name = "Message")
    public void setMessage(String message) {
        this.message = message;
    }
}
