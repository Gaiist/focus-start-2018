package ru.cft.focus.start.koreneva.task5.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import ru.cft.focus.start.koreneva.task5.common.CommandConverter;
import ru.cft.focus.start.koreneva.task5.common.commands.server.DisconnectServerCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.common.helpers.ArgumentHelper;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ICommandConverter;
import ru.cft.focus.start.koreneva.task5.common.network.TCPConnection;
import ru.cft.focus.start.koreneva.task5.server.core.ICommandsHandler;
import ru.cft.focus.start.koreneva.task5.server.core.IUserListener;

public class UserListener implements IUserListener {
    private static final Logger logger = LoggerFactory.getLogger("UserListener");

    private final UserConnection currentUserConnection;
    private final UserConnections userConnections;
    private final ICommandConverter commandConverter;
    private final ICommandsHandler commandsHandler;

    UserListener(UserConnection currentUserConnection, UserConnections userConnections) {
        ArgumentHelper.ensureNotNull(currentUserConnection, "currentUserConnection");
        ArgumentHelper.ensureNotNull(userConnections, "userConnections");

        this.currentUserConnection = currentUserConnection;
        this.userConnections = userConnections;
        commandConverter = new CommandConverter();
        commandsHandler = new CommandsHandler();
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        logger.info("Server connection ready");
    }

    @Override
    public synchronized void onReceiveString(TCPConnection tcpConnection, String receivedString) {
        ArgumentHelper.ensureNotNull(tcpConnection, "tcpConnection");
        ArgumentHelper.ensureNotNullOrEmpty(receivedString, "receivedString");

        try {
            ServerCommand command = commandConverter.deserializeServerCommand(receivedString);
            commandsHandler.handle(currentUserConnection, userConnections, command);
        } catch (SAXException e) {
            logger.info("log");
        }
    }

    @Override
    public synchronized void onDisconnect(TCPConnection tcpConnection) {
        ServerCommand command = new DisconnectServerCommand();
        commandsHandler.handle(currentUserConnection, userConnections, command);
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception e) {
        logger.error("TCPConnection exception: {}", e);
    }
}
