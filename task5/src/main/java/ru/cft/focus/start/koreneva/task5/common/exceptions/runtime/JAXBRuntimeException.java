package ru.cft.focus.start.koreneva.task5.common.exceptions.runtime;

import javax.xml.bind.JAXBException;

public class JAXBRuntimeException extends RuntimeException {
    public JAXBRuntimeException(JAXBException exception) {
        super(exception);
    }
}
