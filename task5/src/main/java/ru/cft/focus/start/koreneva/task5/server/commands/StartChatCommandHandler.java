package ru.cft.focus.start.koreneva.task5.server.commands;

import ru.cft.focus.start.koreneva.task5.common.commands.client.AuthorizedNameClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.client.UserConnectedClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.StartChatServerCommand;
import ru.cft.focus.start.koreneva.task5.common.exceptions.runtime.ErrorRuntimeException;
import ru.cft.focus.start.koreneva.task5.server.UserConnection;
import ru.cft.focus.start.koreneva.task5.server.UserConnections;
import ru.cft.focus.start.koreneva.task5.server.core.ICommandHandler;

import java.io.IOException;

public class StartChatCommandHandler implements ICommandHandler {
    @Override
    public void handle(UserConnection userConnection, UserConnections userConnections, ServerCommand command) {
        StartChatServerCommand startChat = (StartChatServerCommand) command;
        String name = startChat.getName();
        boolean isFree = userConnections.checkFreeName(name);
        AuthorizedNameClientCommand authorizedNameClientCommand = new AuthorizedNameClientCommand();

        authorizedNameClientCommand.setAuthorized(isFree);
        authorizedNameClientCommand.setUserList(userConnections.getUserNameList());
        try {
            userConnection.send(authorizedNameClientCommand);
            if (isFree) {
                userConnection.getUser().setName(name);
                UserConnectedClientCommand userConnectedClientCommand = new UserConnectedClientCommand();
                userConnectedClientCommand.setName(name);
                userConnectedClientCommand.setConnected(isFree);
                userConnectedClientCommand.setUserList(userConnections.getUserNameList());
                userConnections.sendAll(userConnection, userConnectedClientCommand);
            }
        } catch (IOException e) {
            throw new ErrorRuntimeException("User connections can't send command");
        }
    }
}
