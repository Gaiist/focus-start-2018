package ru.cft.focus.start.koreneva.task5.client.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class UserInfoDialog {
    private static final String TITLE_WELCOME = "Welcome!";
    private static final String ENTER_VALUE = "Enter value...";
    private final JTextField fieldNickName;
    private final JTextField fieldPort;


    UserInfoDialog() {
        fieldNickName = new JTextField(ENTER_VALUE);
        fieldPort = new JTextField(ENTER_VALUE);

        setColorAndValue(fieldNickName);
        setColorAndValue(fieldPort);

        fieldNickName.addMouseListener(getMouseListener(fieldNickName));
        fieldPort.addMouseListener(getMouseListener(fieldPort));
    }

    private MouseAdapter getMouseListener(JTextField jTextField) {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                jTextField.setText("");
                jTextField.setForeground(Color.black);
            }
        };
    }

    UserInfoDialogResult showConfirmDialog() {
        Object[] fields = {
                "name", fieldNickName,
                "Server address(host:port)", fieldPort
        };

        int choice = JOptionPane.showConfirmDialog(
                null,
                fields,
                TITLE_WELCOME,
                JOptionPane.OK_CANCEL_OPTION);

        if (choice == JOptionPane.CANCEL_OPTION) {
            System.exit(0);
        }

        return new UserInfoDialogResult(fieldNickName.getText(), fieldPort.getText());
    }

    private void setColorAndValue(JTextField jTextField) {
        jTextField.setText(ENTER_VALUE);
        jTextField.setForeground(Color.red);
    }
}
