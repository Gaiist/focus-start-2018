package ru.cft.focus.start.koreneva.task5.server;

import ru.cft.focus.start.koreneva.task5.common.CommandConverter;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ICommandConverter;
import ru.cft.focus.start.koreneva.task5.common.network.TCPConnection;
import ru.cft.focus.start.koreneva.task5.server.core.IUserConnection;
import ru.cft.focus.start.koreneva.task5.server.core.IUserListener;

import java.io.IOException;
import java.net.Socket;

public class UserConnection implements IUserConnection, Runnable {
    private final User user;
    private final TCPConnection tcpConnection;
    private final ICommandConverter commandConverter;
    private final IUserListener userListener;

    UserConnection(Socket socket, UserConnections userConnections) {
        this.user = new User();
        userListener = new UserListener(this, userConnections);
        commandConverter = new CommandConverter();
        this.tcpConnection = new TCPConnection(userListener, socket);
    }

    public User getUser() {
        return user;
    }

    @Override
    public void send(ClientCommand command) throws IOException {
        String message = commandConverter.serialize(command);
        tcpConnection.send(message);
    }

    @Override
    public void run() {
        try {
            this.tcpConnection.listen();
        } catch (IOException e) {
            userListener.onException(tcpConnection, e);
        }
    }
}
