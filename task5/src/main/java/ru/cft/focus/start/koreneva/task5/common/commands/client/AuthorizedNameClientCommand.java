package ru.cft.focus.start.koreneva.task5.common.commands.client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "AuthorizedNameClientCommand")
public class AuthorizedNameClientCommand extends ClientCommand {
    private boolean authorized;
    private List <String> userList = new ArrayList <>();

    public AuthorizedNameClientCommand() {
        setCommandType(ClientCommandType.AUTHORIZED_NAME);
    }

    public boolean getAuthorized() {
        return authorized;
    }

    @XmlElement(name = "Authorized")
    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public List <String> getUserList() {
        return userList;
    }

    @XmlElement(name = "UserList")
    public void setUserList(List <String> userList) {
        this.userList = userList;
    }
}
