package ru.cft.focus.start.koreneva.task5.common.commands.server;

public enum ServerCommandType {
    START_CHAT,
    SEND_MESSAGE,
    DISCONNECT,
    CONTINUE_CHAT
}
