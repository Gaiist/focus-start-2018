package ru.cft.focus.start.koreneva.task5.common.interfaces;

import java.io.IOException;

public interface ITCPConnection {
    void listen() throws IOException;

    void send(String message) throws IOException;
}
