package ru.cft.focus.start.koreneva.task5.server.commands;

import ru.cft.focus.start.koreneva.task5.common.commands.client.UserActivatedClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ContinueChatServerCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.common.exceptions.runtime.ErrorRuntimeException;
import ru.cft.focus.start.koreneva.task5.server.UserConnection;
import ru.cft.focus.start.koreneva.task5.server.UserConnections;
import ru.cft.focus.start.koreneva.task5.server.core.ICommandHandler;

import java.io.IOException;

public class ContinueChatCommandHandler implements ICommandHandler {
    @Override
    public void handle(UserConnection userConnection, UserConnections userConnections, ServerCommand command) {
        ContinueChatServerCommand continueChatServerCommand = (ContinueChatServerCommand) command;
        String name = continueChatServerCommand.getName();
        boolean isFree = userConnections.checkFreeName(name);

        try {
            if (isFree) {
                userConnection.getUser().setName(name);
                UserActivatedClientCommand userActivatedClientCommand = new UserActivatedClientCommand();
                userActivatedClientCommand.setContinue(isFree);
                userActivatedClientCommand.setUserList(userConnections.getUserNameList());

                userConnections.sendAll(userConnection, userActivatedClientCommand);
            } else {
                UserActivatedClientCommand userActivatedClientCommand = new UserActivatedClientCommand();
                userActivatedClientCommand.setContinue(isFree);

                userConnection.send(userActivatedClientCommand);
            }
        } catch (IOException e) {
            throw new ErrorRuntimeException("User connections can't send command");
        }
    }
}
