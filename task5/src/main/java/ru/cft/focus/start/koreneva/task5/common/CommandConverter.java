package ru.cft.focus.start.koreneva.task5.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.cft.focus.start.koreneva.task5.common.commands.client.*;
import ru.cft.focus.start.koreneva.task5.common.commands.server.*;
import ru.cft.focus.start.koreneva.task5.common.exceptions.runtime.ErrorRuntimeException;
import ru.cft.focus.start.koreneva.task5.common.exceptions.runtime.JAXBRuntimeException;
import ru.cft.focus.start.koreneva.task5.common.helpers.ArgumentHelper;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ICommandConverter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.EnumMap;

public class CommandConverter implements ICommandConverter {
    private static final Logger logger = LoggerFactory.getLogger("CommandConverter");

    private final EnumMap <ServerCommandType, Class> serverClassMap;
    private final EnumMap <ClientCommandType, Class> clientClassMap;

    public CommandConverter() {
        serverClassMap = new EnumMap <>(ServerCommandType.class);
        clientClassMap = new EnumMap <>(ClientCommandType.class);
        registerCommands();
    }

    private void registerCommands() {
        serverClassMap.put(ServerCommandType.SEND_MESSAGE, SendMessageServerCommand.class);
        serverClassMap.put(ServerCommandType.START_CHAT, StartChatServerCommand.class);
        serverClassMap.put(ServerCommandType.DISCONNECT, DisconnectServerCommand.class);
        serverClassMap.put(ServerCommandType.CONTINUE_CHAT, ContinueChatServerCommand.class);

        clientClassMap.put(ClientCommandType.ADD_MESSAGE, AddMessageClientCommand.class);
        clientClassMap.put(ClientCommandType.USER_CONNECTED, UserConnectedClientCommand.class);
        clientClassMap.put(ClientCommandType.AUTHORIZED_NAME, AuthorizedNameClientCommand.class);
        clientClassMap.put(ClientCommandType.USER_ACTIVATED, UserActivatedClientCommand.class);
    }

    @Override
    public String serialize(ServerCommand command) {
        ArgumentHelper.ensureNotNull(command, "command");

        return serializeObject(command);
    }

    private String serializeObject(Object command) {
        try {
            StringWriter writer = new StringWriter();
            Marshaller marshaller = JAXBContext.newInstance(command.getClass())
                    .createMarshaller();
            marshaller.marshal(command, writer);

            return writer.toString();
        } catch (JAXBException e) {
            logger.error("Error when serialize object.", e);
            throw new JAXBRuntimeException(e);
        }
    }

    @Override
    public ServerCommand deserializeServerCommand(String xmlString) throws SAXException {
        ArgumentHelper.ensureNotNullOrEmpty(xmlString, "xmlString");

        try {
            ServerCommandType commandType = ServerCommandType.valueOf(getCommandType(xmlString));
            Class clazz = serverClassMap.get(commandType);

            return (ServerCommand) deserializeCommand(xmlString, clazz);
        } catch (JAXBException e) {
            throw new JAXBRuntimeException(e);
        }
    }

    @Override
    public String serialize(ClientCommand command) {
        ArgumentHelper.ensureNotNull(command, "command");

        return serializeObject(command);
    }

    @Override
    public ClientCommand deserializeClientCommand(String xmlString) throws SAXException {
        ArgumentHelper.ensureNotNullOrEmpty(xmlString, "xmlString");

        try {
            ClientCommandType commandType = ClientCommandType.valueOf(getCommandType(xmlString));
            Class clazz = clientClassMap.get(commandType);

            return (ClientCommand) deserializeCommand(xmlString, clazz);
        } catch (JAXBException e) {
            throw new JAXBRuntimeException(e);
        }
    }

    private Object deserializeCommand(String xmlString, Class clazz) throws JAXBException {
        Unmarshaller unmarshaller = JAXBContext.newInstance(clazz)
                .createUnmarshaller();

        return unmarshaller.unmarshal(new StringReader(xmlString));
    }

    private String getCommandType(String commandXmlString) throws SAXException {
        try {
            InputSource inputSource = new InputSource(new StringReader(commandXmlString));
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(inputSource);
            logger.info("Root element: " + document.getDocumentElement());

            return ( (Element) document.getFirstChild() )
                    .getElementsByTagName("Type")
                    .item(0)
                    .getTextContent();
        } catch (ParserConfigurationException | IOException e) {
            String errorMessage = "Error with getting command type from command.";
            logger.error(errorMessage, e);
            throw new ErrorRuntimeException(errorMessage, e);
        }
    }
}
