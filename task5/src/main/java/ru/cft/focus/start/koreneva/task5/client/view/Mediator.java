package ru.cft.focus.start.koreneva.task5.client.view;

import ru.cft.focus.start.koreneva.task5.client.interfaces.IChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IComponent;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IMediator;
import ru.cft.focus.start.koreneva.task5.client.view.components.InputField;
import ru.cft.focus.start.koreneva.task5.client.view.components.MessagesArea;
import ru.cft.focus.start.koreneva.task5.client.view.components.SendButton;
import ru.cft.focus.start.koreneva.task5.common.PropertiesService;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Mediator implements IMediator {
    private final IChatService connection;
    private final PropertiesService properties;
    private final UserInfoDialog userInfoDialog;
    private final DefaultListModel <String> defaultListModel;
    private final JList <String> users;
    private final JLabel jLabelName;

    private InputField inputField;
    private MessagesArea messagesArea;
    private SendButton sendButton;
    private String nickName;


    public Mediator(IChatService connection) {
        this.connection = connection;
        properties = new PropertiesService();
        userInfoDialog = new UserInfoDialog();
        defaultListModel = new DefaultListModel <>();
        users = new JList <>(defaultListModel);
        jLabelName = new JLabel();
    }

    @Override
    public void registerComponent(IComponent component) {
        component.setConnection(connection);
        switch (component.getType()) {
            case MESSAGE_FIELD:
                messagesArea = (MessagesArea) component;
                break;
            case INPUT_FIELD:
                inputField = (InputField) component;
                break;
            case SEND_BUTTON:
                sendButton = (SendButton) component;
                break;
        }
    }

    @Override
    public UserInfoDialogResult getResultStartDialog() {
        return userInfoDialog.showConfirmDialog();
    }

    @Override
    public void createGUI() {
        JFrame jFrame = new JFrame("Chat");
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(properties.getWidth(), properties.getHeight());
        jFrame.setLocationRelativeTo(null);
        jFrame.setAlwaysOnTop(true);

        initializeBarPanel(jFrame);
        initializeSendPanel(jFrame);
        initializeCenterScrollPane(jFrame);

        jFrame.setVisible(true);
    }

    @Override
    public void setUsers(List <String> userList) {
        defaultListModel.clear();
        if (userList.isEmpty()) {
            defaultListModel.addElement(nickName);
        } else {
            for (String anUserList : userList) {
                defaultListModel.addElement(anUserList);
            }
        }
    }


    public String getMessage() {
        return inputField.getText();
    }

    public void clearInputField() {
        inputField.clearFieldText();
    }

    @Override
    public void displayMessage(String message) {
        messagesArea.printMessage(message);
    }

    @Override
    public void setNickname(String nickName) {
        this.nickName = nickName;
    }

    private void displayNickname() {
        jLabelName.setText(nickName);
    }

    private void initializeBarPanel(JFrame jFrame) {
        JPanel jPanel = new JPanel();
        jPanel.setSize(600, 60);
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.X_AXIS));
        ImageIcon image = new ImageIcon(getClass().getResource("/img/img.png"));
        JLabel jLabel = new JLabel(image);
        jPanel.add(jLabel);
        jPanel.setPreferredSize(new Dimension(70, 60));
        displayNickname();

        jPanel.add(jLabelName);
        jFrame.add(jPanel, BorderLayout.NORTH);
    }

    private void initializeSendPanel(JFrame jFrame) {
        JPanel jPanel = new JPanel();
        jPanel.setSize(600, 20);
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.X_AXIS));
        jPanel.add(inputField.getSwingComponent());

        jPanel.setPreferredSize(new Dimension(530, 20));
        jPanel.add(sendButton.getSwingComponent());
        jFrame.add(jPanel, BorderLayout.SOUTH);
    }

    private void initializeCenterScrollPane(JFrame jFrame) {
        JTextArea jTextArea = messagesArea.getTextArea();
        JScrollPane jScrollPane = new JScrollPane(users);
        users.setVisibleRowCount(5);

        jFrame.add(new JScrollPane(jTextArea), BorderLayout.CENTER);
        jFrame.add(jScrollPane, BorderLayout.WEST);
    }

    @Override
    public void showErrorDialog(String message) {
        UserErrorDialog userErrorDialog = new UserErrorDialog(message);
        userErrorDialog.showErrorPane();
    }
}
