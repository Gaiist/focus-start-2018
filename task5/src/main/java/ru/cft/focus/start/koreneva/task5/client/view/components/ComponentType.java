package ru.cft.focus.start.koreneva.task5.client.view.components;

public enum ComponentType {
    INPUT_FIELD,
    MESSAGE_FIELD,
    SEND_BUTTON
}
