package ru.cft.focus.start.koreneva.task5.common.helpers;

public class StringHelper {
    private StringHelper() {
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }
}
