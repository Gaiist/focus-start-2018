package ru.cft.focus.start.koreneva.task5.server.commands;

import ru.cft.focus.start.koreneva.task5.common.commands.client.AddMessageClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.SendMessageServerCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.common.exceptions.runtime.ErrorRuntimeException;
import ru.cft.focus.start.koreneva.task5.server.UserConnection;
import ru.cft.focus.start.koreneva.task5.server.UserConnections;
import ru.cft.focus.start.koreneva.task5.server.core.ICommandHandler;

import java.io.IOException;

public class SendMessageCommandHandler implements ICommandHandler {
    @Override
    public void handle(UserConnection userConnection, UserConnections userConnections, ServerCommand command) {
        SendMessageServerCommand sendMessageCommand = (SendMessageServerCommand) command;
        String message = sendMessageCommand.getMessage();
        String date = sendMessageCommand.getDate();

        AddMessageClientCommand addMessageCommand = new AddMessageClientCommand();
        addMessageCommand.setName(userConnection.getUser().getName());
        addMessageCommand.setDate(date);
        addMessageCommand.setMessage(message);

        try {
            userConnections.sendAll(userConnection, addMessageCommand);
        } catch (IOException e) {
            throw new ErrorRuntimeException("User connections can't send command");
        }
    }
}
