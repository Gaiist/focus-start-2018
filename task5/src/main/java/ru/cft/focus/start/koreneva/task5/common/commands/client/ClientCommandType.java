package ru.cft.focus.start.koreneva.task5.common.commands.client;

public enum ClientCommandType {
    ADD_MESSAGE,
    USER_CONNECTED,
    AUTHORIZED_NAME,
    USER_ACTIVATED
}
