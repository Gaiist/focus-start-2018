package ru.cft.focus.start.koreneva.task5.client.commands;

import ru.cft.focus.start.koreneva.task5.client.ChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.ICommandHandler;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.client.UserConnectedClientCommand;

public class UserConnectedCommandHandler implements ICommandHandler {
    @Override
    public void handle(ChatService mediator, ClientCommand command) {
        UserConnectedClientCommand userConnectedClientCommand = (UserConnectedClientCommand) command;
        String name = userConnectedClientCommand.getName();

        if (userConnectedClientCommand.isConnected()) {
            mediator.setUserList(userConnectedClientCommand.getUserList());
            mediator.establishMessage(name + " is connected");
            mediator.displayUsers();
        } else {
            mediator.removeUser(name);
            mediator.establishMessage(name + " is disconnected");
            mediator.displayUsers();
        }
    }
}
