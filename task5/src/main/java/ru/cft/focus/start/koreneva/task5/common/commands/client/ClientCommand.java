package ru.cft.focus.start.koreneva.task5.common.commands.client;

import javax.xml.bind.annotation.XmlElement;

public class ClientCommand {
    private ClientCommandType commandType;

    public ClientCommandType getCommandType() {
        return commandType;
    }

    @XmlElement(name = "Type")
    public void setCommandType(ClientCommandType commandType) {
        this.commandType = commandType;
    }
}



