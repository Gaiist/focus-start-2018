package ru.cft.focus.start.koreneva.task5.common.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task5.common.helpers.StringHelper;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ITCPConnection;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ITCPConnectionListener;

import java.io.*;
import java.net.Socket;

public class TCPConnection implements ITCPConnection {
    private static final Logger logger = LoggerFactory.getLogger("TCPConnection");

    private final ITCPConnectionListener eventListener;
    private final Socket socket;

    public TCPConnection(ITCPConnectionListener eventListener, Socket socket) {
        this.eventListener = eventListener;
        this.socket = socket;
    }

    @Override
    public void listen() throws IOException {
        try (BufferedReader reader = createBufferedStreamReader()) {
            logger.info("onConnectionReady...");
            eventListener.onConnectionReady(TCPConnection.this);
            logger.info("onConnectionReady");

            while (!socket.isClosed()) {
                logger.info("Socket.isConnected");
                String message = reader.readLine();
                logger.info("message: {}", message);

                if (StringHelper.isNullOrEmpty(message) && !socket.isClosed()) {
                    disconnect();
                    break;
                }
                eventListener.onReceiveString(TCPConnection.this, message);
            }
        } catch (IOException e) {
            logger.error("Error with receive message.", e);
            socket.close();
            throw e;
        }
    }

    @Override
    public synchronized void send(String message) throws IOException {
        if (StringHelper.isNullOrEmpty(message)) {
            throw new IllegalArgumentException("Message can't be null.");
        }
        try {
            BufferedWriter writer = createBufferedStreamWriter();
            writer.write(message + "\r\n");
            writer.flush();
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
            disconnect();
            logger.error("Error with sending message.", e);
            throw e;
        }
    }

    private void disconnect() {
        eventListener.onDisconnect(this);
    }

    @Override
    public String toString() {
        return "TCPConnection: " + socket.getInetAddress() + " : " + socket.getPort();
    }

    private BufferedReader createBufferedStreamReader() throws IOException {
        InputStream inputStream = socket.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        return new BufferedReader(inputStreamReader);
    }

    private BufferedWriter createBufferedStreamWriter() throws IOException {
        OutputStream inputStream = socket.getOutputStream();
        OutputStreamWriter inputStreamReader = new OutputStreamWriter(inputStream);
        return new BufferedWriter(inputStreamReader);
    }
}
