package ru.cft.focus.start.koreneva.task5.client.view.components;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class InputFieldChangeTextAdapter implements DocumentListener {
    private final IInputFieldChangeTextListener inputFieldChangeTextListener;

    InputFieldChangeTextAdapter(IInputFieldChangeTextListener inputFieldChangeTextListener) {
        this.inputFieldChangeTextListener = inputFieldChangeTextListener;
    }

    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        inputFieldChangeTextListener.onChangeText(documentEvent);
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        inputFieldChangeTextListener.onChangeText(documentEvent);
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        inputFieldChangeTextListener.onChangeText(documentEvent);
    }

    @FunctionalInterface
    public interface IInputFieldChangeTextListener {
        void onChangeText(DocumentEvent documentEvent);
    }
}
