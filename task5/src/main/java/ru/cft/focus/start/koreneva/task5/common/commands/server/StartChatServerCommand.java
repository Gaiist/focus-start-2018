package ru.cft.focus.start.koreneva.task5.common.commands.server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "StartChatServerCommand")
public class StartChatServerCommand extends ServerCommand {
    private String name;

    public StartChatServerCommand() {
        setCommandType(ServerCommandType.START_CHAT);
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "Name")
    public void setName(String name) {
        this.name = name;
    }
}
