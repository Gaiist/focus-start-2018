package ru.cft.focus.start.koreneva.task5.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IMediator;
import ru.cft.focus.start.koreneva.task5.client.view.Mediator;
import ru.cft.focus.start.koreneva.task5.client.view.UserInfoDialogResult;
import ru.cft.focus.start.koreneva.task5.client.view.components.InputField;
import ru.cft.focus.start.koreneva.task5.client.view.components.MessagesArea;
import ru.cft.focus.start.koreneva.task5.client.view.components.SendButton;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ContinueChatServerCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.SendMessageServerCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.StartChatServerCommand;
import ru.cft.focus.start.koreneva.task5.common.helpers.DateHelper;
import ru.cft.focus.start.koreneva.task5.common.helpers.StringHelper;
import ru.cft.focus.start.koreneva.task5.common.helpers.ThreadHelper;

import java.io.IOException;
import java.net.Socket;
import java.util.Date;
import java.util.List;

public class ChatService implements IChatService {
    private static Logger logger = LoggerFactory.getLogger("ChatService");

    private final IMediator mediator;

    private List <String> userList;
    private UserConnection userConnection;
    private String nickName;
    private String message;
    private int port;
    private String host;

    public ChatService() {
        mediator = new Mediator(this);
    }

    @Override
    public void tryStartChat() {
        mediator.registerComponent(new InputField());
        mediator.registerComponent(new SendButton());
        mediator.registerComponent(new MessagesArea());

        setNameAndAddress();
        boolean success = tryConnect();
        if (!success) {
            mediator.showErrorDialog("ChatService isn't established.");
            System.exit(0);
        }
        sendStartChatMessage();
    }

    private void sendStartChatMessage() {
        StartChatServerCommand startChatServerCommand = new StartChatServerCommand();
        startChatServerCommand.setName(nickName);
        userConnection.send(startChatServerCommand);
    }

    private void setNameAndAddress() {
        UserInfoDialogResult userInfoDialogResult = mediator.getResultStartDialog();
        nickName = userInfoDialogResult.getNickName();
        String[] address = userInfoDialogResult.getAddress().split(":");
        host = address[0];
        String stringPort = address[1];

        if (StringHelper.isNullOrEmpty(nickName) || nickName.equals("Enter value...") || StringHelper.isNullOrEmpty(stringPort) || stringPort.equals("Enter value...")) {
            mediator.showErrorDialog("Name or port can't be null or empty.");
            setNameAndAddress();
        } else {
            try {
                port = Integer.parseInt(stringPort);
                mediator.setNickname(nickName);
            } catch (NumberFormatException e) {
                logger.info("Parse exception with port{}", port);
                mediator.showErrorDialog("Port must contain only digits.");
                setNameAndAddress();
            }
        }
    }

    @Override
    public void changeName(boolean isFirstTry) {
        mediator.showErrorDialog(nickName + " is exist.");
        setNameAndAddress();
        if (isFirstTry) {
            StartChatServerCommand startChatServerCommand = new StartChatServerCommand();
            startChatServerCommand.setName(nickName);
            userConnection.send(startChatServerCommand);
        } else {
            ContinueChatServerCommand continueChatServerCommand = new ContinueChatServerCommand();
            continueChatServerCommand.setName(nickName);
            userConnection.send(continueChatServerCommand);
        }
    }

    @Override
    public void setUserList(List <String> userList) {
        this.userList = userList;
        mediator.setUsers(userList);
    }

    @Override
    public void removeUser(String name) {
        userList.remove(name);
    }

    @Override
    public void sendNewMessage() {
        if (message.equals("")) {
            return;
        }
        message = mediator.getMessage();

        SendMessageServerCommand sendMessageServerCommand = new SendMessageServerCommand();
        sendMessageServerCommand.setMessage(message);
        sendMessageServerCommand.setDate(DateHelper.getUTCDate(new Date()));
        userConnection.send(sendMessageServerCommand);

        mediator.clearInputField();
    }

    @Override
    public boolean makeFiveConnectAttempting() {
        mediator.showErrorDialog("ChatService to the server is lost. Please wait. \n" +
                "We're trying to reconnect.");
        for (int i = 0; i < 5; i++) {
            if (tryConnect()) {
                ContinueChatServerCommand continueChatServerCommand = new ContinueChatServerCommand();
                continueChatServerCommand.setName(nickName);
                userConnection.send(continueChatServerCommand);

                return true;
            }

            logger.info("Wait connection.");
            ThreadHelper.trySleep(5000);
        }

        return false;
    }

    @Override
    public void showChatWindow() {
        mediator.createGUI();
    }

    @Override
    public void establishMessage(String message) {
        mediator.displayMessage(message);
    }

    @Override
    public void displayUsers() {
        mediator.setUsers(userList);
    }

    @Override
    public void changeNickname() {
        mediator.setNickname(nickName);
    }


    private boolean tryConnect() {
        try {
            Socket socket = new Socket(host, port);
            userConnection = new UserConnection(this, socket);
            Thread thread = new Thread(userConnection);
            thread.start();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public void updateMessage(String message) {
        this.message = message;
    }
}
