package ru.cft.focus.start.koreneva.task5.client.interfaces;

import ru.cft.focus.start.koreneva.task5.client.view.components.ComponentType;

import java.awt.*;

public interface IComponent {
    void setConnection(IChatService connection);

    ComponentType getType();

    Component getSwingComponent();
}
