package ru.cft.focus.start.koreneva.task5.common.exceptions.runtime;

public class ErrorRuntimeException extends RuntimeException {
    public ErrorRuntimeException() {
    }

    public ErrorRuntimeException(String s) {
        super(s);
    }

    public ErrorRuntimeException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ErrorRuntimeException(Throwable throwable) {
        super(throwable);
    }
}
