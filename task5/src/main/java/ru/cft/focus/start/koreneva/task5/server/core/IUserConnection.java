package ru.cft.focus.start.koreneva.task5.server.core;

import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.server.User;

import java.io.IOException;

public interface IUserConnection {
    User getUser();

    void send(ClientCommand command) throws IOException;
}
