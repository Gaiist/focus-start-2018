package ru.cft.focus.start.koreneva.task5.client.view.components;

import ru.cft.focus.start.koreneva.task5.client.interfaces.IChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IComponent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import java.awt.*;
import java.awt.event.KeyEvent;

public class InputField implements IComponent, InputFieldChangeTextAdapter.IInputFieldChangeTextListener,
        KeyPressAdapter.IKeyPressListener {
    private final JTextField textField;
    private IChatService connection;

    public InputField() {
        textField = new JTextField();
        setUpComponent();
    }

    private void setUpComponent() {
        InputFieldChangeTextAdapter changeTextAdapter =
                new InputFieldChangeTextAdapter(this);
        textField.getDocument().addDocumentListener(changeTextAdapter);

        KeyPressAdapter keyTypedAdapter = new KeyPressAdapter(this);
        textField.addKeyListener(keyTypedAdapter);

        textField.setColumns(46);
        textField.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "tynth");
    }

    public void setConnection(IChatService connection) {
        this.connection = connection;
    }

    @Override
    public void onChangeText(DocumentEvent documentEvent) {
        connection.updateMessage(textField.getText());
    }

    @Override
    public void onKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
            connection.sendNewMessage();
        }
    }

    @Override
    public ComponentType getType() {
        return ComponentType.INPUT_FIELD;
    }

    @Override
    public Component getSwingComponent() {
        return this.textField;
    }

    public String getText() {
        return this.textField.getText();
    }

    public void clearFieldText() {
        textField.setText("");
    }
}
