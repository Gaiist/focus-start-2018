package ru.cft.focus.start.koreneva.task5.client;

import ru.cft.focus.start.koreneva.task5.client.commands.AddMessageCommandHandler;
import ru.cft.focus.start.koreneva.task5.client.commands.AuthorizedNameCommandHandler;
import ru.cft.focus.start.koreneva.task5.client.commands.UserActivatedCommandHandler;
import ru.cft.focus.start.koreneva.task5.client.commands.UserConnectedCommandHandler;
import ru.cft.focus.start.koreneva.task5.client.interfaces.ICommandHandler;
import ru.cft.focus.start.koreneva.task5.client.interfaces.ICommandsHandler;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommandType;

import java.util.EnumMap;

public class CommandsHandler implements ICommandsHandler {
    private final ChatService mediator;
    private final EnumMap <ClientCommandType, ICommandHandler> handlerMap;

    CommandsHandler(ChatService mediator) {
        this.mediator = mediator;
        handlerMap = new EnumMap <>(ClientCommandType.class);
        registerCommands();
    }

    private void registerCommands() {
        handlerMap.put(ClientCommandType.ADD_MESSAGE, new AddMessageCommandHandler());
        handlerMap.put(ClientCommandType.USER_CONNECTED, new UserConnectedCommandHandler());
        handlerMap.put(ClientCommandType.AUTHORIZED_NAME, new AuthorizedNameCommandHandler());
        handlerMap.put(ClientCommandType.USER_ACTIVATED, new UserActivatedCommandHandler());
    }

    @Override
    public void handle(ClientCommand command) {
        handlerMap.get(command.getCommandType())
                .handle(mediator, command);
    }
}
