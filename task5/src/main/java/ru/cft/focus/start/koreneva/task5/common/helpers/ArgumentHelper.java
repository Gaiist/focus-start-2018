package ru.cft.focus.start.koreneva.task5.common.helpers;

public class ArgumentHelper {
    private ArgumentHelper() {
    }

    public static void ensureNotNull(Object notNullObject, String paramName) {
        if (notNullObject == null) {
            String message = String.format("%s can't be null.", paramName);
            throw new IllegalArgumentException(message);
        }
    }

    public static void ensureNotNullOrEmpty(String notNullOrEmptyString, String paramName) {
        if (StringHelper.isNullOrEmpty(notNullOrEmptyString)) {
            String message = String.format("%s can't be null or empty.", paramName);
            throw new IllegalArgumentException(message);
        }
    }
}
