package ru.cft.focus.start.koreneva.task5.client.interfaces;

import java.util.List;

public interface IChatService {
    void tryStartChat();

    void changeName(boolean isFirstTry);

    void setUserList(List <String> userList);

    void removeUser(String name);

    void sendNewMessage();

    boolean makeFiveConnectAttempting();

    void showChatWindow();

    void establishMessage(String message);

    void displayUsers();

    void changeNickname();

    void updateMessage(String message);
}
