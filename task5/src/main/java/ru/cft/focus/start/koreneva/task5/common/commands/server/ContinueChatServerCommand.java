package ru.cft.focus.start.koreneva.task5.common.commands.server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ContinueChatServerCommand")
public class ContinueChatServerCommand extends ServerCommand {
    private String name;

    public ContinueChatServerCommand() {
        setCommandType(ServerCommandType.CONTINUE_CHAT);
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "Name")
    public void setName(String name) {
        this.name = name;
    }
}
