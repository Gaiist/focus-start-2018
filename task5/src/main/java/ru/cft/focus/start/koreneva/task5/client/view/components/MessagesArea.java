package ru.cft.focus.start.koreneva.task5.client.view.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IComponent;

import javax.swing.*;
import java.awt.*;

public class MessagesArea implements IComponent {
    private static final Logger logger = LoggerFactory.getLogger("MessagesArea");

    private final JTextArea textArea;

    public MessagesArea() {
        textArea = new JTextArea();
        logger.info("JTextArea Hash: {}", textArea.hashCode());
        textArea.setEditable(false);
        textArea.setLineWrap(true);
    }

    @Override
    public void setConnection(IChatService connection) {
        //Do nothing case field mediator isn't required. But class MessagesArea is component.
    }

    public void printMessage(String message) {
        logger.info("printMessage: {}", message);

        SwingUtilities.invokeLater(() -> {
            logger.info("invoke later: {}", message);
            textArea.append(message + "\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
        });
    }

    @Override
    public ComponentType getType() {
        return ComponentType.MESSAGE_FIELD;
    }

    @Override
    public Component getSwingComponent() {
        logger.info("Get swing component: {}", this.textArea.hashCode());
        return this.textArea;
    }

    public JTextArea getTextArea() {
        return this.textArea;
    }
}
