package ru.cft.focus.start.koreneva.task5.common.commands.client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "UserActivatedClientCommand")
public class UserActivatedClientCommand extends ClientCommand {
    private boolean isContinue;
    private List <String> userList = new ArrayList <>();

    public UserActivatedClientCommand() {
        setCommandType(ClientCommandType.USER_ACTIVATED);
    }

    public boolean getContinue() {
        return isContinue;
    }

    @XmlElement(name = "IsContinue")
    public void setContinue(boolean aContinue) {
        isContinue = aContinue;
    }

    public List <String> getUserList() {
        return userList;
    }

    @XmlElement(name = "UserList")
    public void setUserList(List <String> userList) {
        this.userList = userList;
    }
}
