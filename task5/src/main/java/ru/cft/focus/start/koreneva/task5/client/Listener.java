package ru.cft.focus.start.koreneva.task5.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import ru.cft.focus.start.koreneva.task5.client.interfaces.ICommandsHandler;
import ru.cft.focus.start.koreneva.task5.common.CommandConverter;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ICommandConverter;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ITCPConnectionListener;
import ru.cft.focus.start.koreneva.task5.common.network.TCPConnection;

public class Listener implements ITCPConnectionListener {
    private static final Logger logger = LoggerFactory.getLogger("Listener");

    private final ICommandsHandler commandsHandler;
    private final ICommandConverter commandConverter;
    private final ChatService mediator;

    Listener(ChatService mediator) {
        commandsHandler = new CommandsHandler(mediator);
        commandConverter = new CommandConverter();
        this.mediator = mediator;
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        logger.info("Client connection is ready {}", tcpConnection);
    }

    @Override
    public synchronized void onReceiveString(TCPConnection tcpConnection, String value) {
        try {
            ClientCommand command = commandConverter.deserializeClientCommand(value);
            commandsHandler.handle(command);
        } catch (SAXException e) {
            onException(tcpConnection, e);
        }
    }

    @Override
    public synchronized void onDisconnect(TCPConnection tcpConnection) {
        if (!mediator.makeFiveConnectAttempting()) {
            System.exit(0);
        }
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception e) {
        logger.error("TCPConnection exception: {}", e);
    }
}
