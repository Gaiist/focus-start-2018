package ru.cft.focus.start.koreneva.task5.common.commands.client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "UserConnectedClientCommand")
public class UserConnectedClientCommand extends ClientCommand {
    private String name;
    private boolean isConnected;
    private List <String> userList = new ArrayList <>();

    public UserConnectedClientCommand() {
        setCommandType(ClientCommandType.USER_CONNECTED);
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "Name")
    public void setName(String name) {
        this.name = name;
    }

    public boolean isConnected() {
        return isConnected;
    }

    @XmlElement(name = "Connected")
    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public List <String> getUserList() {
        return userList;
    }

    @XmlElement(name = "UserList")
    public void setUserList(List <String> userList) {
        this.userList = userList;
    }
}
