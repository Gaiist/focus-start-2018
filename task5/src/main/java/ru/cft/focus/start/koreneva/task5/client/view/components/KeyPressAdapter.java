package ru.cft.focus.start.koreneva.task5.client.view.components;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyPressAdapter extends KeyAdapter {
    private final IKeyPressListener keyTypedListener;

    KeyPressAdapter(IKeyPressListener keyTypedListener) {
        this.keyTypedListener = keyTypedListener;
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        this.keyTypedListener.onKeyPressed(keyEvent);
    }

    @FunctionalInterface
    public interface IKeyPressListener {
        void onKeyPressed(KeyEvent keyEvent);
    }
}
