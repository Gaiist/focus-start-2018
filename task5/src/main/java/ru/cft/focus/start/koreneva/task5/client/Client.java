package ru.cft.focus.start.koreneva.task5.client;

import javax.swing.*;

public class Client {
    public static void main(String[] args) {
        ChatService connection = new ChatService();
        SwingUtilities.invokeLater(connection::tryStartChat);
    }
}