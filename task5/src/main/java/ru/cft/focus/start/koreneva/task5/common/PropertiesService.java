package ru.cft.focus.start.koreneva.task5.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesService {
    private static final Logger logger = LoggerFactory.getLogger("PropertiesService");

    private final Properties properties;

    public PropertiesService() {
        properties = new Properties();
        loadProperties();
    }

    public int getPort() {
        return Integer.parseInt(properties.getProperty("PORT"));
    }

    public int getWidth() {
        return Integer.parseInt(properties.getProperty("WIDTH"));
    }

    public int getHeight() {
        return Integer.parseInt(properties.getProperty("HEIGHT"));
    }

    private void loadProperties() {
        String file = getClass().getResource("/server.properties").getFile();
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            properties.load(fileInputStream);
        } catch (IOException e) {
            logger.error("File {} not found.", file);
        }
    }
}
