package ru.cft.focus.start.koreneva.task5.common.interfaces;

import org.xml.sax.SAXException;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;

public interface ICommandConverter {
    String serialize(ServerCommand command);

    ServerCommand deserializeServerCommand(String value) throws SAXException;

    String serialize(ClientCommand command);

    ClientCommand deserializeClientCommand(String value) throws SAXException;
}
