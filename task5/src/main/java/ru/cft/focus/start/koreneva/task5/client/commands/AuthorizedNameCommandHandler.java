package ru.cft.focus.start.koreneva.task5.client.commands;

import ru.cft.focus.start.koreneva.task5.client.ChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.ICommandHandler;
import ru.cft.focus.start.koreneva.task5.common.commands.client.AuthorizedNameClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;

public class AuthorizedNameCommandHandler implements ICommandHandler {
    @Override
    public void handle(ChatService connection, ClientCommand command) {
        AuthorizedNameClientCommand authorizedNameClientCommand = (AuthorizedNameClientCommand) command;
        if (authorizedNameClientCommand.getAuthorized()) {
            connection.setUserList(authorizedNameClientCommand.getUserList());
            connection.showChatWindow();
        } else {
            connection.changeName(true);
        }
    }
}
