package ru.cft.focus.start.koreneva.task5.server;

import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommandType;
import ru.cft.focus.start.koreneva.task5.server.commands.ContinueChatCommandHandler;
import ru.cft.focus.start.koreneva.task5.server.commands.DisconnectCommandHandler;
import ru.cft.focus.start.koreneva.task5.server.commands.SendMessageCommandHandler;
import ru.cft.focus.start.koreneva.task5.server.commands.StartChatCommandHandler;
import ru.cft.focus.start.koreneva.task5.server.core.ICommandHandler;
import ru.cft.focus.start.koreneva.task5.server.core.ICommandsHandler;

import java.util.EnumMap;

public class CommandsHandler implements ICommandsHandler {
    private final EnumMap <ServerCommandType, ICommandHandler> handlerMap;

    CommandsHandler() {
        handlerMap = new EnumMap <>(ServerCommandType.class);
        registerCommands();
    }

    private void registerCommands() {
        handlerMap.put(ServerCommandType.START_CHAT, new StartChatCommandHandler());
        handlerMap.put(ServerCommandType.SEND_MESSAGE, new SendMessageCommandHandler());
        handlerMap.put(ServerCommandType.DISCONNECT, new DisconnectCommandHandler());
        handlerMap.put(ServerCommandType.CONTINUE_CHAT, new ContinueChatCommandHandler());
    }

    @Override
    public void handle(UserConnection userConnection, UserConnections userConnections, ServerCommand command) {
        handlerMap.get(command.getCommandType())
                .handle(userConnection, userConnections, command);
    }
}
