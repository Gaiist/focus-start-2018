package ru.cft.focus.start.koreneva.task5.common.commands.client;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "AddMessageClientCommand")
public class AddMessageClientCommand extends ClientCommand {
    private String name;
    private String message;
    private String date;

    public AddMessageClientCommand() {
        setCommandType(ClientCommandType.ADD_MESSAGE);
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "Name")
    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    @XmlElement(name = "Message")
    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    @XmlElement(name = "Date")
    public void setDate(String date) {
        this.date = date;
    }
}
