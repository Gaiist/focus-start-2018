package ru.cft.focus.start.koreneva.task5.server.core;

import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.server.UserConnection;

import java.io.IOException;

public interface IUserConnections {
    void listen();

    void stopListen();

    void sendAll(UserConnection userConnection, ClientCommand command) throws IOException;
}
