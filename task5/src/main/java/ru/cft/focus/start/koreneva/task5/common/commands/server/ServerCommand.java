package ru.cft.focus.start.koreneva.task5.common.commands.server;

import javax.xml.bind.annotation.XmlElement;

public class ServerCommand {
    private ServerCommandType commandType;

    public ServerCommandType getCommandType() {
        return commandType;
    }

    @XmlElement(name = "Type")
    public void setCommandType(ServerCommandType commandType) {
        this.commandType = commandType;
    }
}
