package ru.cft.focus.start.koreneva.task5.client.interfaces;

import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;

public interface IUserConnection {
    void send(ServerCommand command);
}
