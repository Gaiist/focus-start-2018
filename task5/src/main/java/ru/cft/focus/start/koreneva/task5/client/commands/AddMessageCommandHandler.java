package ru.cft.focus.start.koreneva.task5.client.commands;

import ru.cft.focus.start.koreneva.task5.client.ChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.ICommandHandler;
import ru.cft.focus.start.koreneva.task5.common.commands.client.AddMessageClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.helpers.DateHelper;

import java.util.Date;

public class AddMessageCommandHandler implements ICommandHandler {
    @Override
    public void handle(ChatService connection, ClientCommand command) {
        AddMessageClientCommand addMessageClientCommand = (AddMessageClientCommand) command;
        Date date = DateHelper.getLocalDate(addMessageClientCommand.getDate());

        String message = String.format("%s %s %s",
                DateHelper.convertToString(date),
                addMessageClientCommand.getName(),
                addMessageClientCommand.getMessage());

        connection.establishMessage(message);
    }
}
