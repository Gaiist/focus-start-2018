package ru.cft.focus.start.koreneva.task5.client.interfaces;

import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;

public interface ICommandsHandler {
    void handle(ClientCommand command);
}
