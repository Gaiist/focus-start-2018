package ru.cft.focus.start.koreneva.task5.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IUserConnection;
import ru.cft.focus.start.koreneva.task5.common.CommandConverter;
import ru.cft.focus.start.koreneva.task5.common.commands.server.ServerCommand;
import ru.cft.focus.start.koreneva.task5.common.interfaces.ITCPConnectionListener;
import ru.cft.focus.start.koreneva.task5.common.network.TCPConnection;

import java.io.IOException;
import java.net.Socket;

public class UserConnection implements IUserConnection, Runnable {
    private static final Logger logger = LoggerFactory.getLogger("UserConnection");

    private final CommandConverter commandConverter;
    private final TCPConnection tcpConnection;


    public UserConnection(ChatService mediator, Socket socket) {
        ITCPConnectionListener eventListener = new Listener(mediator);
        commandConverter = new CommandConverter();
        tcpConnection = new TCPConnection(eventListener, socket);
    }

    @Override
    public void send(ServerCommand command) {
        try {
            tcpConnection.send(commandConverter.serialize(command));
        } catch (IOException e) {
            logger.error("Message can be send.");
        }
    }

    @Override
    public void run() {
        try {
            this.tcpConnection.listen();
        } catch (IOException e) {
            logger.error("Fatal error.");
        }
    }
}
