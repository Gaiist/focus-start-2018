package ru.cft.focus.start.koreneva.task5.common.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateHelper {
    private static final String DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";

    private DateHelper() {
    }

    private static DateFormat getUTCDateFormat() {
        DateFormat dateFormatUTC = new SimpleDateFormat(DATE_FORMAT_STRING);
        dateFormatUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormatUTC;
    }

    public static String getUTCDate(Date date) {
        return getUTCDateFormat().format(date);
    }

    public static Date getLocalDate(String date) {
        try {
            return getUTCDateFormat().parse(date);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Can't pare system date.");
        }
    }

    public static String convertToString(Date date) {
        DateFormat format = new SimpleDateFormat(DATE_FORMAT_STRING);
        return format.format(date);
    }
}
