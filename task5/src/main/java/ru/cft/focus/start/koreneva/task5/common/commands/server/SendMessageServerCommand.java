package ru.cft.focus.start.koreneva.task5.common.commands.server;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SendMessageServerCommand")
public class SendMessageServerCommand extends ServerCommand {
    private String message;
    private String date;

    public SendMessageServerCommand() {
        setCommandType(ServerCommandType.SEND_MESSAGE);
    }

    public String getMessage() {
        return message;
    }

    @XmlElement(name = "Message")
    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    @XmlElement(name = "Date")
    public void setDate(String date) {
        this.date = date;
    }
}
