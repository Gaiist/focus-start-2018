package ru.cft.focus.start.koreneva.task5.client.commands;

import ru.cft.focus.start.koreneva.task5.client.ChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.ICommandHandler;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.commands.client.UserActivatedClientCommand;

public class UserActivatedCommandHandler implements ICommandHandler {
    @Override
    public void handle(ChatService connection, ClientCommand command) {
        UserActivatedClientCommand userActivatedClientCommand = (UserActivatedClientCommand) command;
        if (!userActivatedClientCommand.getContinue()) {
            connection.changeName(false);
        } else {
            connection.setUserList(userActivatedClientCommand.getUserList());
            connection.displayUsers();
            connection.changeNickname();
        }
    }
}
