package ru.cft.focus.start.koreneva.task5.client.view.components;

import ru.cft.focus.start.koreneva.task5.client.interfaces.IChatService;
import ru.cft.focus.start.koreneva.task5.client.interfaces.IComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class SendButton implements IComponent {
    private final JButton button;

    private IChatService connection;

    public SendButton() {
        ImageIcon image = new ImageIcon(getClass().getResource("/img/send.png"));
        this.button = new JButton(image);

        setUpComponent();
    }

    private void setUpComponent() {
        this.button.addActionListener(this::fireActionPerformed);
        button.setPreferredSize(new Dimension(70, 20));
    }

    @Override
    public void setConnection(IChatService connection) {
        this.connection = connection;
    }

    private void fireActionPerformed(ActionEvent actionEvent) {
        connection.sendNewMessage();
    }

    @Override
    public ComponentType getType() {
        return ComponentType.SEND_BUTTON;
    }

    @Override
    public Component getSwingComponent() {
        return this.button;
    }
}
