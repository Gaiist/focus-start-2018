package ru.cft.focus.start.koreneva.task5.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task5.common.PropertiesService;
import ru.cft.focus.start.koreneva.task5.common.commands.client.ClientCommand;
import ru.cft.focus.start.koreneva.task5.common.helpers.ArgumentHelper;
import ru.cft.focus.start.koreneva.task5.server.core.IUserConnections;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserConnections implements IUserConnections {
    private static Logger logger = LoggerFactory.getLogger("UserConnections");

    private final List <UserConnection> userConnectionList;
    private final PropertiesService propertiesService;
    private boolean isListen = true;

    public UserConnections() {
        userConnectionList = new ArrayList <>();
        propertiesService = new PropertiesService();
    }

    @Override
    public void listen() {
        logger.info("Server running...");

        try (ServerSocket serverSocket = new ServerSocket(propertiesService.getPort())) {
            while (isListen) {
                UserConnection connection = new UserConnection(serverSocket.accept(), this);
                Thread thread = new Thread(connection);
                userConnectionList.add(connection);
                thread.start();
            }
        } catch (IOException e) {
            logger.error("Error when listen new connections.", e);
            isListen = false;
        }
    }

    @Override
    public void stopListen() {
        isListen = false;
    }

    @Override
    public void sendAll(UserConnection userConnection, ClientCommand command) throws IOException {
        for (UserConnection connection : userConnectionList) {
            logger.info("UserConnection = {}, User = {}", connection, connection.getUser());
            connection.send(command);
        }
    }

    public void removeConnection(UserConnection userConnection) {
        userConnectionList.remove(userConnection);
    }

    public synchronized boolean checkFreeName(String name) {
        ArgumentHelper.ensureNotNullOrEmpty(name, "name");

        return userConnectionList.stream()
                .map(UserConnection::getUser)
                .filter(User::isActive)
                .noneMatch(user -> name.equals(user.getName()));
    }

    public synchronized List <String> getUserNameList() {
        return userConnectionList.stream()
                .map(userConnection -> userConnection.getUser().getName())
                .collect(Collectors.toList());
    }
}
