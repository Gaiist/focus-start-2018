package ru.cft.focus.start.koreneva.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    private static final int MAX_SIZE_OF_TABLE = 32;
    private static final int MIN_SIZE_OF_TABLE = 1;
    private static int sizeOfTable;

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Please, enter a number.");
            sizeOfTable = Integer.parseInt(bufferedReader.readLine());
            if (sizeOfTable < MIN_SIZE_OF_TABLE || sizeOfTable > MAX_SIZE_OF_TABLE) {
                throw new SizeOfTableException();
            }
        } catch (SizeOfTableException | NumberFormatException exception) {
            System.out.printf("You entered an invalid value. Please enter a number between %d and %d.", MIN_SIZE_OF_TABLE, MAX_SIZE_OF_TABLE);
            return;
        } catch (IOException exception) {
            System.out.print("Fatal error.");
        }

        PrintMultiplicationTableService print = new PrintMultiplicationTableService(sizeOfTable);

        print.printTable();
    }
}

