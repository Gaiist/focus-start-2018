package ru.cft.focus.start.koreneva.task1;

class PrintMultiplicationTableService {
    private final int tableSize;
    private int lengthMaxNumber;

    PrintMultiplicationTableService(int tableSize) {
        this.tableSize = tableSize;
    }

    void printTable() {
        lengthMaxNumber = lengthOfNumber(tableSize * tableSize);
        String dashes = getDashes();

        int lengthOfNumber;
        int number;

        printFirstLine(dashes);

        for (int i = 1; i < tableSize + 1; i++) {
            printSpaces(2 - lengthOfNumber(i));
            System.out.print(i + "|");

            for (int j = 1; j < tableSize; j++) {
                number = i * j;
                lengthOfNumber = lengthOfNumber(number);

                printSpaces(lengthMaxNumber - lengthOfNumber);
                System.out.print(number + "|");
            }
            printSpaces(lengthMaxNumber - lengthOfNumber(tableSize * i));
            System.out.print(tableSize * i);
            System.out.println();
            System.out.println(dashes);
        }
    }

    private void printFirstLine(String dashes) {
        int numberLength;
        System.out.print("  |");

        for (int i = 1; i < tableSize; i++) {
            numberLength = lengthOfNumber(i);
            printSpaces(lengthMaxNumber - numberLength);

            System.out.print(i + "|");
        }
        numberLength = lengthOfNumber(tableSize);
        printSpaces(lengthMaxNumber - numberLength);
        System.out.print(tableSize);
        System.out.println();

        System.out.println(dashes);
    }

    private void printSpaces(int size) {
        for (int j = 0; j < size; j++) {
            System.out.print(" ");
        }
    }

    private String getDashes() {
        StringBuilder countAllDashes = new StringBuilder();
        StringBuilder dashLine = new StringBuilder();

        for (int i = 0; i < lengthMaxNumber; i++) {
            countAllDashes = countAllDashes.append("-");
        }

        dashLine.append("--+");

        for (int i = 0; i < tableSize - 1; i++) {
            dashLine.append(countAllDashes);
            dashLine.append("+");
        }
        dashLine.append(countAllDashes);

        return dashLine.toString();
    }

    private int lengthOfNumber(int number) {
        return String.valueOf(number).length();
    }
}