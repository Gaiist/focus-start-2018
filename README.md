# FOCUS START 2018

## Задачи

| # | Описание задания |
| ----- | ---- |
| 1 | Вывести в консоль таблицу умножения для N числа. Входной параметр - размер таблицы (N) |
| 2 | фигуры. Тип фигуры и ее параметры читаются из файлаю Для разных типов фигур параметры отличаются. Результатом работы приложения являются вычислительные характеристики фигуры. Для разных фигур хар-ки отличаются, также они зависят от введенных параметров фигуры. |
| 3 | сапер. Архитектура программы основана на паттерне MVC |
| 4.1 | вычисляет сложную функцию (сходящийся ряд) от 1 до 1000000 параллельно |
| 4.2 | многопоточное производство | 
| 5 | клиент-серверное приложение - чат с UI-интерфейсом клиента |

## Автор
Кристина Коренева
