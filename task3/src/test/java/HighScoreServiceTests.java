import helpers.AssertHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.cft.focus.start.koreneva.task3.Level;
import ru.cft.focus.start.koreneva.task3.model.HighScoreService;
import ru.cft.focus.start.koreneva.task3.model.Score;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IHighScoreRepository;

import java.util.Date;

public class HighScoreServiceTests {
    private static final int SCORE_LIMIT = 40;
    private HighScoreService highScoreService;
    private IHighScoreRepository highScoreRepository;

    @Before
    public void before() {
        highScoreRepository = new MockHighScoreRepository();
        highScoreService = new HighScoreService(highScoreRepository, SCORE_LIMIT);
    }

    @Test
    public void add() {
        Score score = new Score(new Date(), "test", Level.PROFESSIONAL, 4);
        highScoreService.add(score);
        Assert.assertNotEquals(-1, score.getId());
        AssertHelper.assertCount(1, highScoreRepository.getAll());

        for (int i = 0; i < SCORE_LIMIT * 3; i++) {
            Score item = new Score(new Date(), "test" + i, Level.LOVER, i);
            highScoreService.add(item);
        }

        AssertHelper.assertCount(SCORE_LIMIT, highScoreRepository.getAll());
    }
}
