import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.cft.focus.start.koreneva.task3.Level;
import ru.cft.focus.start.koreneva.task3.helpers.MatrixHelper;
import ru.cft.focus.start.koreneva.task3.model.GameService;
import ru.cft.focus.start.koreneva.task3.model.IMatrixGenerator;
import ru.cft.focus.start.koreneva.task3.model.Score;
import ru.cft.focus.start.koreneva.task3.model.TimeService;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IHighScoreService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class GameServiceTests {
    private GameService gameService;
    private MockView observer;

    @Before
    public void setUp() {
        TimeService timeService = new TimeService();
        observer = new MockView();
        IHighScoreService highScoreService = new IHighScoreService() {
            @Override
            public void add(Score score) {
            }

            @Override
            public String getHighScoreInfoAsString() {
                return "";
            }
        };
        IMatrixGenerator matrixGenerator = (noBombX, noBombY, sizeX, sizeY, trueCount) -> {
            if (sizeX == 9 && sizeY == 9) {
                boolean[][] results = new boolean[sizeX][sizeY];
                for (int i = 0; i < 9; i++) {
                    results[0][i] = true;
                }
                results[1][0] = true;
                return results;
            } else {
                throw new NotImplementedException();
            }
        };

        gameService = new GameService(timeService, highScoreService, matrixGenerator);
        gameService.addEventListener(observer);
    }

    @Test
    public void tryStartNewGame() {
        gameService.tryStartNewGame();
        Assert.assertEquals(9, observer.pointMatrix.length);
        Assert.assertEquals(9, observer.pointMatrix[0].length);
        for (MockView.Point point : MatrixHelper.toList(observer.pointMatrix)) {
            Assert.assertEquals(MockView.PointType.CLOSED, point.type);
        }
        Assert.assertEquals(10, observer.minesCount);

        gameService.tryStartNewGame();
        Assert.assertEquals(9, observer.pointMatrix.length);
        Assert.assertEquals(9, observer.pointMatrix[0].length);
        for (MockView.Point point : MatrixHelper.toList(observer.pointMatrix)) {
            Assert.assertEquals(MockView.PointType.CLOSED, point.type);
        }
        Assert.assertEquals(10, observer.minesCount);
    }

    @Test
    public void tryOpen() {
        gameService.tryStartNewGame();

        try {
            gameService.tryOpen(-1, -1);
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
            // ignored
        }

        int x = 5;
        int y = 5;
        gameService.tryOpen(x, y);
        MockView.Point point = observer.pointMatrix[x][y];
        Assert.assertEquals(MockView.PointType.EMPTY, point.type);
        Assert.assertEquals(0, point.aroundBombCount);

        x = 1;
        y = 1;
        gameService.tryOpen(x, y);
        point = observer.pointMatrix[x][y];
        Assert.assertEquals(MockView.PointType.EMPTY, point.type);
        Assert.assertEquals(4, point.aroundBombCount);

        x = 0;
        y = 0;
        gameService.tryOpen(x, y);
        point = observer.pointMatrix[x][y];
        Assert.assertEquals(MockView.PointType.BOMB, point.type);
        Assert.assertTrue(observer.looser);
    }

    @Test
    public void tryMarkAsBomb() {
        gameService.tryStartNewGame();

        int x = 0;
        int y = 0;
        gameService.tryMarkAsBomb(x, y);
        MockView.Point point = observer.pointMatrix[x][y];
        Assert.assertEquals(MockView.PointType.FLAGGED, point.type);

        gameService.tryMarkAsBomb(x, y);
        point = observer.pointMatrix[x][y];
        Assert.assertEquals(MockView.PointType.CLOSED, point.type);
    }

    @Test
    public void tryChangeLevel() {
        gameService.tryStartNewGame();
        Level level = Level.PROFESSIONAL;
        gameService.tryChangeLevel(level);
        Assert.assertEquals(16, observer.pointMatrix.length);
        Assert.assertEquals(30, observer.pointMatrix[0].length);
    }

    @Test
    public void tryChangeBombCount() {
        gameService.tryStartNewGame();

        try {
            gameService.tryChangeBombCount(-1);
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
            // ignored
        }

        try {
            gameService.tryChangeBombCount(85);
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
            // ignored
        }

        int bombCount = 15;
        gameService.tryChangeBombCount(15);
        Assert.assertEquals(bombCount, observer.minesCount);
    }

    @Test
    public void tryOpenHighScore() {
        gameService.tryOpenHighScore();
        Assert.assertEquals("", observer.highScores);
    }

    @Test
    public void trySetName() {
        try {
            gameService.trySetName(null);
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
            // ignored
        }

        try {
            gameService.trySetName("");
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
            // ignored
        }

        gameService.trySetName("name");
    }
}
