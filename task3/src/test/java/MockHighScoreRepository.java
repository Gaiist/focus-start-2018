import ru.cft.focus.start.koreneva.task3.model.Score;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IHighScoreRepository;

import java.util.ArrayList;
import java.util.List;

public class MockHighScoreRepository implements IHighScoreRepository {
    private final List <Score> data;

    private long idCounter;

    MockHighScoreRepository() {
        data = new ArrayList <>();

        this.idCounter = 0;
    }

    @Override
    public List <Score> getAll() {
        return data;
    }

    @Override
    public void add(Score score) {
        score.setId(idCounter++);
        data.add(score);
    }

    @Override
    public void delete(long id) {
        Score score = data.stream()
                .filter(s -> s.getId() == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("test."));
        data.remove(score);
    }
}
