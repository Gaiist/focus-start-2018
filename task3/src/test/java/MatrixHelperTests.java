import org.junit.Assert;
import org.junit.Test;
import ru.cft.focus.start.koreneva.task3.helpers.MatrixHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MatrixHelperTests {
    @Test
    public void getAroundItems() {
        Integer[][] matrix = {{1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4}};

        List <Integer> result = MatrixHelper.getAroundItems(matrix, 1, 1);
        List <Integer> around = new ArrayList <>();
        for (int x = 0; x < 3; x++) {
            around.addAll(Arrays.asList(matrix[x]).subList(0, 3));
        }

        Assert.assertEquals(result, around);
        around.clear();

        result = MatrixHelper.getAroundItems(matrix, 0, 0);
        for (int x = 0; x < 2; x++) {
            around.addAll(Arrays.asList(matrix[x]).subList(0, 2));
        }

        Assert.assertEquals(result, around);
        around.clear();

        result = MatrixHelper.getAroundItems(matrix, 0, 1);
        for (int x = 0; x < 2; x++) {
            around.addAll(Arrays.asList(matrix[x]).subList(0, 3));
        }

        Assert.assertEquals(result, around);
        around.clear();

        result = MatrixHelper.getAroundItems(matrix, 0, 3);
        for (int x = 0; x < 2; x++) {
            around.addAll(Arrays.asList(matrix[x]).subList(2, 4));
        }

        Assert.assertEquals(result, around);
        around.clear();

        result = MatrixHelper.getAroundItems(matrix, 3, 0);
        for (int x = 2; x < 4; x++) {
            around.addAll(Arrays.asList(matrix[x]).subList(0, 2));
        }
        Assert.assertEquals(result, around);
        around.clear();

        result = MatrixHelper.getAroundItems(matrix, 3, 3);
        for (int x = 2; x < 4; x++) {
            around.addAll(Arrays.asList(matrix[x]).subList(2, 4));
        }

        Assert.assertEquals(result, around);

        testFailedGet(matrix, 0, -1);
        testFailedGet(matrix, -1, 0);
        testFailedGet(matrix, 5, 0);
        testFailedGet(matrix, 0, 7);
    }

    @Test
    public void convertToObjectBoolean() {
        boolean[][] matrix = {{true, false}, {true, true}, {false, true}};
        Boolean[][] result = MatrixHelper.convertToObjectBoolean(matrix);

        Assert.assertArrayEquals(result, matrix);
    }

    private void testFailedGet(Integer[][] matrix, int x, int y) {
        try {
            MatrixHelper.getAroundItems(matrix, x, y);
            Assert.fail();
        } catch (IndexOutOfBoundsException ignored) {
            // ignored
        }
    }
}
