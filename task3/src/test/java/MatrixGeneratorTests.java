import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.cft.focus.start.koreneva.task3.model.MatrixGenerator;

public class MatrixGeneratorTests {
    private MatrixGenerator matrixGenerator;

    @Before
    public void setUp() {
        matrixGenerator = new MatrixGenerator();
    }

    @Test
    public void generateBooleanMatrix() {
        testFailedGeneration(0, 1, -1, 5, 1);
        testFailedGeneration(5, 5, 5, -5, 1);
        testFailedGeneration(5, 0, 5, 5, -5);
        testFailedGeneration(0, 0, 5, 5, 50);
        testFailedGeneration(-1, 0, 5, 5, 50);
        testFailedGeneration(0, -1, 5, 5, 50);

        int sizeX = 9;
        int sizeY = 9;
        int trueCount = 10;
        int trueCounter = 0;

        boolean[][] result = matrixGenerator.generateBooleanMatrix(0, 0, sizeX, sizeY, trueCount);
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                if (result[x][y]) {
                    trueCounter++;
                }
            }
        }

        Assert.assertEquals(trueCount, trueCounter);
    }

    private void testFailedGeneration(int x, int y, int sizeX, int sizeY, int trueCount) {
        try {
            matrixGenerator.generateBooleanMatrix(x, y, sizeX, sizeY, trueCount);
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
            // ignored
        }
    }
}
