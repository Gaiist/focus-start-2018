import org.junit.Assert;
import org.junit.Test;
import ru.cft.focus.start.koreneva.task3.helpers.TimeHelper;

public class TimeHelperTests {
    @Test
    public void fromSeconds() {
        String time = "65:30";
        String result = TimeHelper.fromTotalSecondsAsString(3930);
        Assert.assertEquals(result, time);

        try {
            TimeHelper.fromTotalSecondsAsString(-10);
            Assert.fail();
        } catch (IllegalArgumentException ignored) {
            // ignored
        }

        time = "01:18";
        result = TimeHelper.fromTotalSecondsAsString(78);

        Assert.assertEquals(result, time);
    }
}
