package helpers;

import org.junit.Assert;

import java.util.List;

public class AssertHelper {
    public static <T> void assertCount(int count, List <T> list) {
        Assert.assertEquals(count, list.size());
    }
}
