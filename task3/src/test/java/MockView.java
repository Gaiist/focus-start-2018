import ru.cft.focus.start.koreneva.task3.model.interfaces.IGameEvents;

public class MockView implements IGameEvents {
    public Point[][] pointMatrix;
    public int minesCount;
    public boolean winner;
    public boolean looser;
    public String highScores;
    public String time;

    @Override
    public void setClosedAll(int sizeX, int sizeY) {
        pointMatrix = new Point[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                pointMatrix[i][j] = new Point(PointType.CLOSED);
            }
        }
    }

    @Override
    public void setClosed(int x, int y) {
        pointMatrix[x][y].setType(PointType.CLOSED);
    }

    @Override
    public void setFlagged(int x, int y) {
        pointMatrix[x][y].setType(PointType.FLAGGED);
    }

    @Override
    public void setBomb(int x, int y) {
        pointMatrix[x][y].setType(PointType.BOMB);
    }

    @Override
    public void setEmpty(int x, int y, int aroundBombCount) {
        pointMatrix[x][y].setEmpty(aroundBombCount);
    }

    @Override
    public void setNoBomb(int x, int y) {
        pointMatrix[x][y].setType(PointType.NOBOMB);
    }

    @Override
    public void setMinesCount(int minesCount) {
        this.minesCount = minesCount;
    }

    @Override
    public void win() {
        winner = true;
    }

    @Override
    public void loose() {
        looser = true;
    }

    @Override
    public void openHighScores(String scores) {
        highScores = scores;
    }

    enum PointType {
        CLOSED,
        NOBOMB,
        FLAGGED,
        EMPTY,
        BOMB
    }

    public static class Point {
        public PointType type;
        public int aroundBombCount;

        public Point(PointType type) {
            this.type = type;
        }

        public void setType(PointType type) {
            this.type = type;
        }

        public void setEmpty(int aroundBombCount) {
            this.type = PointType.EMPTY;
            this.aroundBombCount = aroundBombCount;
        }
    }
}
