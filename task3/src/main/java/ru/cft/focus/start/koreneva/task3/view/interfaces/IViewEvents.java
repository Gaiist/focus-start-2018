package ru.cft.focus.start.koreneva.task3.view.interfaces;

import ru.cft.focus.start.koreneva.task3.Level;

public interface IViewEvents {
    void tryStartNewGame();

    void tryOpen(int x, int y);

    void tryOpenEmptyPointMiddleClicked(int x, int y);

    void tryMarkAsBomb(int x, int y);

    void tryChangeLevel(Level level);

    void tryChangeBombCount(int bombCount);

    void tryOpenHighScore();

    void trySetName(String name);

    void tryUpdateTime();
}
