package ru.cft.focus.start.koreneva.task3.model;

import java.util.Random;

public class MatrixGenerator implements IMatrixGenerator {
    private final Random random;

    public MatrixGenerator() {
        this.random = new Random();
    }

    @Override
    public boolean[][] generateBooleanMatrix(int noBombX, int noBombY, int sizeX, int sizeY, int trueCount) {
        if (sizeX <= 0 || sizeY <= 0 || noBombX < 0 || noBombY < 0) {
            throw new IllegalArgumentException("Sizes can't be less or equal then 0.");
        }

        if (trueCount <= 0 || trueCount > ( sizeX * sizeY )) {
            throw new IllegalArgumentException("Count of true values can't be less or equal then 0. " +
                    "And more then items count.");
        }

        boolean[][] result = new boolean[sizeX][sizeY];
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                result[x][y] = false;
            }
        }

        for (int i = 0; i < trueCount; i++) {
            int x = random.nextInt(sizeX);
            int y = random.nextInt(sizeY);

            if (result[x][y] || ( x == noBombX && y == noBombY )) {
                Point emptyPoint = getFalseCoordinates(noBombX, noBombY, result, x, y, sizeX, sizeY);
                result[emptyPoint.getX()][emptyPoint.getY()] = true;
                continue;
            }
            result[x][y] = true;
        }

        return result;
    }

    private Point getFalseCoordinates(int noBombX, int noBombY, boolean[][] matrix, int currentX, int currentY, int sizeX, int sizeY) {
        for (int x = currentX; x < sizeX; x++) {
            for (int y = currentY; y < sizeY; y++) {
                if (x == noBombX && y == noBombY) {
                    continue;
                }
                if (!matrix[x][y]) {
                    return new Point(x, y);
                }
            }
        }
        return getFalseCoordinates(noBombX, noBombY, matrix, 0, 0, sizeX, sizeY);
    }

    private static class Point {
        private final int x;
        private final int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int getX() {
            return x;
        }

        int getY() {
            return y;
        }
    }
}
