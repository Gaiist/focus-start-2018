package ru.cft.focus.start.koreneva.task3.model;

import ru.cft.focus.start.koreneva.task3.helpers.TimeHelper;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IHighScoreRepository;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IHighScoreService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class HighScoreService implements IHighScoreService {
    private final DateFormat dateFormat;
    private final IHighScoreRepository highScoreRepository;
    private final int scoreLimit;

    public HighScoreService(IHighScoreRepository highScoreRepository, int scoreLimit) {
        this.highScoreRepository = highScoreRepository;
        this.scoreLimit = scoreLimit;

        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public void add(Score score) {
        highScoreRepository.add(score);

        List <Score> currentScores = highScoreRepository.getAll();
        int removeItemsCount = currentScores.size() > scoreLimit ? currentScores.size() - scoreLimit : 0;
        List <Score> removeScores = currentScores.stream()
                .sorted((item1, item2) -> Long.compare(item2.getTotalSeconds(), item1.getTotalSeconds()))
                .limit(removeItemsCount)
                .collect(Collectors.toList());

        for (Score removeScore : removeScores) {
            highScoreRepository.delete(removeScore.getId());
        }
    }

    @Override
    public String getHighScoreInfoAsString() {
        StringBuilder builder = new StringBuilder();

        for (Score score : highScoreRepository.getAll()) {
            String time = TimeHelper.fromTotalSecondsAsString(score.getTotalSeconds());
            String formattedDate = dateFormat.format(score.getDate());

            builder.append(String.format("[%s] %s %s %s", score.getLevel(), score.getName(), time, formattedDate));
            builder.append(System.lineSeparator());
        }

        return builder.toString();
    }
}
