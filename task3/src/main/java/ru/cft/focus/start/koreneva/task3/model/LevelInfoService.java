package ru.cft.focus.start.koreneva.task3.model;

import ru.cft.focus.start.koreneva.task3.Level;

import java.util.EnumMap;

class LevelInfoService {
    private final EnumMap <Level, LevelInfo> levelInfoDictionary;

    LevelInfoService() {
        levelInfoDictionary = new EnumMap <>(Level.class);
        levelInfoDictionary.put(Level.BEGINNER, new LevelInfo(Level.BEGINNER, 9, 9, 10));
        levelInfoDictionary.put(Level.LOVER, new LevelInfo(Level.LOVER, 16, 16, 40));
        levelInfoDictionary.put(Level.PROFESSIONAL, new LevelInfo(Level.PROFESSIONAL, 16, 30, 99));
    }

    LevelInfo get(Level level) {
        return levelInfoDictionary.get(level);
    }

    static class LevelInfo {
        private final Level level;
        private final int sizeX;
        private final int sizeY;
        private final int bombCount;

        LevelInfo(Level level, int sizeX, int sizeY, int bombCount) {
            this.level = level;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
            this.bombCount = bombCount;
        }

        int getSizeX() {
            return sizeX;
        }

        int getSizeY() {
            return sizeY;
        }

        Level getLevel() {
            return level;
        }

        int getBombCount() {
            return bombCount;
        }
    }
}
