package ru.cft.focus.start.koreneva.task3.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task3.view.interfaces.IViewEvents;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class JLabelMouseAdapter extends MouseAdapter {
    private final Logger logger;
    private final int x;
    private final int y;
    private final List <IViewEvents> viewEventsList;

    JLabelMouseAdapter(int x, int y, List <IViewEvents> viewEventsList) {
        logger = LoggerFactory.getLogger(JLabelMouseAdapter.class);
        this.x = x;
        this.y = y;
        this.viewEventsList = viewEventsList;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
            logger.info("in mouseClicked left");
            viewEventsList.forEach(viewEvents -> viewEvents.tryOpen(x, y));
        } else if (SwingUtilities.isRightMouseButton(mouseEvent)) {
            logger.info("in mouseClicked right");
            viewEventsList.forEach(viewEvents -> viewEvents.tryMarkAsBomb(x, y));
        } else if (SwingUtilities.isMiddleMouseButton(mouseEvent)) {
            logger.info("in mouseClicked middle");
            viewEventsList.forEach(viewEvents -> viewEvents.tryOpenEmptyPointMiddleClicked(x, y));
        }
    }
}
