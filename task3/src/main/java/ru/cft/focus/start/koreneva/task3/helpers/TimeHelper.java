package ru.cft.focus.start.koreneva.task3.helpers;

public class TimeHelper {
    private TimeHelper() {
    }

    public static String fromTotalSecondsAsString(long totalSeconds) {
        if (totalSeconds < 0) {
            throw new IllegalArgumentException("Total seconds can't be less then 0.");
        }

        int minutes = (int) totalSeconds / 60;
        int seconds = (int) totalSeconds - ( minutes * 60 );

        return String.format("%02d:%02d", minutes, seconds);
    }
}