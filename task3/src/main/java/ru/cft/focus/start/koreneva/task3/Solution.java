package ru.cft.focus.start.koreneva.task3;

import ru.cft.focus.start.koreneva.task3.controller.Controller;
import ru.cft.focus.start.koreneva.task3.model.GameService;
import ru.cft.focus.start.koreneva.task3.model.HighScoreService;
import ru.cft.focus.start.koreneva.task3.model.MatrixGenerator;
import ru.cft.focus.start.koreneva.task3.model.TimeService;
import ru.cft.focus.start.koreneva.task3.repositories.JsonFileHighScoreRepository;
import ru.cft.focus.start.koreneva.task3.view.View;

public class Solution {
    public static void main(String[] args) {
        new Solution().runGame();
    }

    private void runGame() {
        TimeService timeService = new TimeService();
        HighScoreService highScoreService = new HighScoreService(new JsonFileHighScoreRepository(), 5);
        GameService gameService = new GameService(timeService, highScoreService, new MatrixGenerator());
        View viewService = new View(timeService);
        Controller controller = new Controller(gameService);
        gameService.addEventListener(viewService);
        viewService.addEventListener(controller);

        controller.tryStartNewGame();
    }
}
