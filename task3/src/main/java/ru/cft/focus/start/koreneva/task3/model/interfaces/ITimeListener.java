package ru.cft.focus.start.koreneva.task3.model.interfaces;

public interface ITimeListener {
    void setTime(String time);
}
