package ru.cft.focus.start.koreneva.task3.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IGameEvents;
import ru.cft.focus.start.koreneva.task3.model.interfaces.ITimeService;
import ru.cft.focus.start.koreneva.task3.view.interfaces.IViewEvents;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class View implements IGameEvents {
    private static final int DEFAULT_IMAGE_SIZE = 20;
    private final Logger logger;

    private final JPanel jButtonPanel;
    private final JPanel jPanelInform = new JPanel();
    private final GridBagLayout gridBagLayout;
    private final GridBagConstraints gridBagConstraints;
    private final java.util.List <IViewEvents> viewEventsList;
    private final JLabel bombCountLabel;
    private final JLabel timerLabel;
    private JFrame jFrame;
    private JButton[][] jButtons;

    public View(ITimeService timeService) {
        logger = LoggerFactory.getLogger(View.class);

        jButtonPanel = new JPanel();
        gridBagLayout = new GridBagLayout();
        gridBagConstraints = new GridBagConstraints();

        viewEventsList = new java.util.ArrayList <>();
        timerLabel = new JLabel();
        bombCountLabel = new JLabel();
        TimeView timeView = new TimeView(timerLabel, viewEventsList);
        timeService.addEventListener(timeView);
        timeView.run();

        makeComponents();
        makeInformPanel();
    }

    @Override
    public void setClosedAll(int sizeX, int sizeY) {
        logger.info("setClosedAll {}", ( jButtons == null ));
        if (jButtons == null) {
            initButtons(sizeX, sizeY);
        }

        if (sizeX != jButtons.length || sizeY != jButtons[0].length) {
            jButtonPanel.removeAll();
            initButtons(sizeX, sizeY);
        }

        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                setJButtonImage(x, y, "closed");
            }
        }
    }

    @Override
    public void setClosed(int x, int y) {
        setJButtonImage(x, y, "closed");
    }


    @Override
    public void setFlagged(int x, int y) {
        setJButtonImage(x, y, "flagged");
    }

    @Override
    public void setBomb(int x, int y) {
        setJButtonImage(x, y, "bomb");
    }

    @Override
    public void setEmpty(int x, int y, int aroundBombCount) {
        setJButtonImage(x, y, Integer.toString(aroundBombCount));
    }

    @Override
    public void setNoBomb(int x, int y) {
        setJButtonImage(x, y, "exception");
    }

    @Override
    public void setMinesCount(int minesCount) {
        bombCountLabel.setText(Integer.toString(minesCount));
    }

    @Override
    public void win() {
        String name = JOptionPane.showInputDialog("What is your name?");
        forEachEvents(viewEvents -> viewEvents.trySetName(name));

        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(
                null,
                "You win. Start new game?",
                "You win",
                dialogButton);

        if (dialogResult == 0) {
            forEachEvents(IViewEvents::tryStartNewGame);
        }
    }

    @Override
    public void loose() {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(
                null,
                "You loose. Start new game?",
                "You loose.",
                dialogButton);

        if (dialogResult == 0) {
            forEachEvents(IViewEvents::tryStartNewGame);
        }
    }

    @Override
    public void openHighScores(String scores) {
        JOptionPane.showMessageDialog(null, scores);
    }

    public void addEventListener(IViewEvents viewEvents) {
        if (viewEvents == null) {
            throw new IllegalArgumentException("View events can't be null.");
        }

        viewEventsList.add(viewEvents);
    }

    private void initButtons(int sizeX, int sizeY) {
        jButtons = new JButton[sizeX][sizeY];
        jButtonPanel.setPreferredSize(new Dimension(sizeY * DEFAULT_IMAGE_SIZE, sizeX * DEFAULT_IMAGE_SIZE));

        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                JButton jButton = new JButton();
                jButton.setPreferredSize(new Dimension(DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE));
                jButtons[x][y] = jButton;

                JLabelMouseAdapter pointMouseAdapter = new JLabelMouseAdapter(x, y, viewEventsList);
                jButton.addMouseListener(pointMouseAdapter);

                gridBagConstraints.gridx = x;
                gridBagConstraints.gridx = y;

                gridBagLayout.setConstraints(jButton, gridBagConstraints);

                jButtonPanel.add(jButton);
            }
        }
        makeLayout();
        setVisible();
    }

    private void setJButtonImage(int x, int y, String name) {
        jButtons[x][y].setIcon(getImage(name));
    }

    private ImageIcon getImage(String name) {
        String filePath = String.format("/icons/%s.png", name);
        URL fileUrl = getClass().getResource(filePath);
        return new ImageIcon(fileUrl);
    }

    private void makeComponents() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        jFrame = new JFrame("MineSweeper");
        jFrame.setIconImage(getImage("icon").getImage());
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jButtonPanel.setLayout(gridBagLayout);

        new MenuBar(jFrame, viewEventsList)
                .configureMenuBar();
    }

    private void makeLayout() {
        jFrame.add(jButtonPanel, BorderLayout.CENTER);
        jFrame.add(jPanelInform, BorderLayout.SOUTH);

        jFrame.pack();
        jFrame.setLocationRelativeTo(null);
    }

    private void makeInformPanel() {
        ImageIcon mineImg = getImage("mine");
        jPanelInform.add(new JLabel(mineImg));
        jPanelInform.add(bombCountLabel);

        ImageIcon clockImg = getImage("clock");
        jPanelInform.add(new JLabel(clockImg));
        jPanelInform.add(timerLabel);
    }

    private void setVisible() {
        jFrame.setVisible(true);
    }

    private void forEachEvents(IViewEventsAction action) {
        for (IViewEvents viewEvents : viewEventsList) {
            action.execute(viewEvents);
        }
    }

    @FunctionalInterface
    private interface IViewEventsAction {
        void execute(IViewEvents viewEvents);
    }
}