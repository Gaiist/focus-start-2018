package ru.cft.focus.start.koreneva.task3.model;

class EmptyPoint extends Point {
    private int aroundBombCount;

    EmptyPoint(int x, int y, int aroundBombCount) {
        super(x, y, PointType.EMPTY);

        if (0 > aroundBombCount || aroundBombCount > 8) {
            throw new IllegalArgumentException("Count of around bombs can't be less than 0 or grater than 8.");
        }

        this.aroundBombCount = aroundBombCount;
    }

    int getAroundBombCount() {
        return aroundBombCount;
    }
}
