package ru.cft.focus.start.koreneva.task3.model;

class BombPoint extends Point {
    BombPoint(int x, int y) {
        super(x, y, PointType.BOMB);
    }
}
