package ru.cft.focus.start.koreneva.task3.model;

public interface IMatrixGenerator {
    boolean[][] generateBooleanMatrix(int noBombX, int noBombY, int sizeX, int sizeY, int trueCount);
}
