package ru.cft.focus.start.koreneva.task3.view;

import ru.cft.focus.start.koreneva.task3.model.interfaces.ITimeListener;
import ru.cft.focus.start.koreneva.task3.view.interfaces.IViewEvents;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.List;

class TimeView implements ITimeListener {
    private final JLabel timeJLabel;
    private final Timer timer;
    private List <IViewEvents> viewEventsList;

    TimeView(JLabel jLabel, List <IViewEvents> viewEventsList) {
        this.timeJLabel = jLabel;
        this.viewEventsList = viewEventsList;
        this.timer = new Timer(100, this::actionPerformed);
    }

    void run() {
        this.timer.start();
    }

    private void actionPerformed(ActionEvent ignored) {
        for (IViewEvents events : viewEventsList) {
            events.tryUpdateTime();
        }
    }

    @Override
    public void setTime(String time) {
        timeJLabel.setText(time);
    }
}
