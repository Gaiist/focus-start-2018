package ru.cft.focus.start.koreneva.task3.model.interfaces;

import ru.cft.focus.start.koreneva.task3.model.Score;

public interface IHighScoreService {
    void add(Score score);

    String getHighScoreInfoAsString();
}
