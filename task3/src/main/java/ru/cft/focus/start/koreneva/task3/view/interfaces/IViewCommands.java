package ru.cft.focus.start.koreneva.task3.view.interfaces;

public interface IViewCommands {
    void setClosedAll(int sizeX, int sizeY);

    void setClosed(int x, int y);

    void setFlagged(int x, int y);

    void setBomb(int x, int y);

    void setEmpty(int x, int y, int aroundBombCount);

    void setNoBomb(int x, int y);

    void setMinesCount(int minesCount);

    void win();

    void loose();

    void openHighScores(String scores);

    void setTimer(String time);
}
