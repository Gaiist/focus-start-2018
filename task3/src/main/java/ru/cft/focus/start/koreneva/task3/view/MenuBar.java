package ru.cft.focus.start.koreneva.task3.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task3.Level;
import ru.cft.focus.start.koreneva.task3.view.interfaces.IViewEvents;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

class MenuBar {
    private final Logger logger;
    private final List <IViewEvents> viewEventsList;
    private final JFrame frame;

    MenuBar(JFrame frame, List <IViewEvents> viewEvents) {
        logger = LoggerFactory.getLogger(MenuBar.class);

        this.frame = frame;
        this.viewEventsList = viewEvents;
    }

    void configureMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu game = new JMenu("Game");
        configureMenuGame(menuBar, game);
        JMenu help = new JMenu("Help");
        configureMenuHelp(menuBar, help);

        frame.setJMenuBar(menuBar);
    }

    private void configureMenuHelp(JMenuBar menuBar, JMenu help) {
        JMenuItem jMenu = new JMenuItem("About");
        StringBuilder messageBuilder = new StringBuilder();
        String path = getClass().getResource("/files/about.txt").getFile();

        help.add(jMenu);

        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            while (reader.ready()) {
                messageBuilder.append(reader.readLine());
                messageBuilder.append(System.lineSeparator());
            }
            jMenu.addActionListener(e -> JOptionPane.showMessageDialog(null, messageBuilder.toString(), "Game rules", JOptionPane.INFORMATION_MESSAGE));
        } catch (IOException ex) {
            logger.error("File not read.");
        }

        menuBar.add(help);
    }

    private void configureMenuGame(JMenuBar menuBar, JMenu game) {
        game.add(getNewGame());
        game.add(getLevels());
        game.add(getBombsNumber());
        game.add(createHighScoreMenuItem());
        game.add(getExit());

        menuBar.add(game);
    }

    private JMenuItem getNewGame() {
        JMenuItem jMenu = new JMenuItem("New game");
        jMenu.addActionListener(e -> {
            for (IViewEvents viewEvents : viewEventsList) {
                viewEvents.tryStartNewGame();
            }
        });

        return jMenu;
    }

    private JMenuItem getLevels() {
        JMenu jMenu = new JMenu("Level");
        Level[] levels = Level.values();
        ButtonGroup group = new ButtonGroup();

        for (int index = 0; index < 3; index++) {
            JRadioButton jRadioButton = new JRadioButton(levels[index].toString().toLowerCase(), false);
            int finalIndex = index;
            jRadioButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent mouseEvent) {
                    logger.info("in mouseClicked {}", levels[finalIndex]);
                    if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
                        logger.info("left mouseClicked {}", levels[finalIndex]);
                        for (IViewEvents viewEvents : viewEventsList) {
                            logger.info("tryChangeLevel {}", levels[finalIndex]);
                            viewEvents.tryChangeLevel(levels[finalIndex]);
                        }
                    }
                    logger.info("out mouseClicked");
                }
            });

            group.add(jRadioButton);
            jMenu.add(jRadioButton);
        }

        return jMenu;
    }

    private JMenuItem getBombsNumber() {
        JMenu bombNumbers = new JMenu("The numbers of bombs");
        ButtonGroup group = new ButtonGroup();

        for (int bombCount = 10; bombCount <= 40; bombCount += 10) {
            JRadioButton jRadioButton = new JRadioButton(String.valueOf(bombCount));
            int finalBombCount = bombCount;
            jRadioButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent mouseEvent) {
                    if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
                        for (IViewEvents viewEvents : viewEventsList) {
                            viewEvents.tryChangeBombCount(finalBombCount);
                        }
                    }
                }
            });

            group.add(jRadioButton);
            bombNumbers.add(jRadioButton);
        }

        return bombNumbers;
    }

    private JMenuItem createHighScoreMenuItem() {
        JMenuItem jMenuItem = new JMenuItem("High Score");
        jMenuItem.addActionListener(e -> {
            for (IViewEvents viewEvents : viewEventsList) {
                viewEvents.tryOpenHighScore();
            }
        });

        return jMenuItem;
    }

    private JMenuItem getExit() {
        JMenuItem jMenu = new JMenuItem("Exit");
        jMenu.addActionListener(e -> frame.dispose());

        return jMenu;
    }
}