package ru.cft.focus.start.koreneva.task3.repositories;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task3.model.Score;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IHighScoreRepository;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class JsonFileHighScoreRepository implements IHighScoreRepository {
    private final String filePath;
    private final Logger logger;
    private final Gson gson;
    private final Type type;

    private long idCounter;

    public JsonFileHighScoreRepository() {
        logger = LoggerFactory.getLogger(getClass());
        filePath = getClass().getResource("/files/highScores.json").getFile();
        gson = new Gson();
        type = new TypeToken <ArrayList <Score>>() {
        }.getType();

        idCounter = calculateIdCounter();
    }

    @Override
    public List <Score> getAll() {
        try (FileReader reader = new FileReader(filePath)) {
            List <Score> result = gson.fromJson(reader, type);
            if (result == null) {
                return new ArrayList <>();
            }

            return result;
        } catch (IOException e) {
            logger.error("Fatal error!", e);
            throw new InternalError(e);
        }
    }

    @Override
    public void add(Score score) {
        List <Score> scores = getAll();
        score.setId(++idCounter);
        scores.add(score);
        rewriteAll(scores);
    }

    @Override
    public void delete(long id) {
        List <Score> scores = getAll();
        Score scoreForDelete = scores.stream()
                .filter(score -> score.getId() == id)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Score with id %d not found", id)));
        scores.remove(scoreForDelete);
    }

    private long calculateIdCounter() {
        return getAll().stream()
                .map(Score::getId)
                .max(Long::compareTo)
                .orElse(-1L);
    }

    private void rewriteAll(List <Score> scores) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath))) {
            String json = gson.toJson(scores);
            bufferedWriter.write(json);
        } catch (IOException e) {
            logger.error("Fatal error!", e);
            throw new InternalError(e);
        }
    }
}
