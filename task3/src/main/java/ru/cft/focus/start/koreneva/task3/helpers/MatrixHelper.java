package ru.cft.focus.start.koreneva.task3.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MatrixHelper {
    private MatrixHelper() {
    }

    public static <T> List <T> getAroundItems(T[][] matrix, int x, int y) {
        if (x < 0 || y < 0) {
            throw new IndexOutOfBoundsException("x or y can't be less then 0.");
        }

        if (x > matrix.length || y > matrix[0].length) {
            throw new IndexOutOfBoundsException("x or y can't be more then {}." + matrix.length);
        }
        List <T> result = new ArrayList <>();

        int startX = getStartIndex(x);
        int startY = getStartIndex(y);
        int endX = getEndIndex(matrix, x);
        int endY = getEndIndex(matrix[0], y);

        for (int i = startX; i <= endX; i++) {
            result.addAll(Arrays.asList(matrix[i]).subList(startY, endY + 1));
        }

        return result;
    }

    public static Boolean[][] convertToObjectBoolean(boolean[][] matrix) {
        Boolean[][] resultMatrix = new Boolean[matrix.length][matrix[0].length];

        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                resultMatrix[x][y] = matrix[x][y];
            }
        }

        return resultMatrix;
    }

    private static int getEndIndex(Object[] array, int index) {
        return ( index > array.length - 2 ) ? array.length - 1 : index + 1;
    }

    private static int getStartIndex(int index) {
        return ( index < 1 ) ? 0 : index - 1;
    }

    public static <T> List <T> toList(T[][] matrix) {
        List <T> result = new ArrayList <>();

        for (T[] array : matrix) {
            Collections.addAll(result, array);
        }

        return result;
    }
}
