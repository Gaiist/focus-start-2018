package ru.cft.focus.start.koreneva.task3.model.interfaces;

public interface ITimeService {
    void addEventListener(ITimeListener event);

    void start();

    void stop();

    long getTotalSeconds();

    boolean isStarted();

    void updateTime();
}
