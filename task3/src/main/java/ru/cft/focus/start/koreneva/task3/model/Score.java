package ru.cft.focus.start.koreneva.task3.model;

import ru.cft.focus.start.koreneva.task3.Level;

import java.util.Date;

public class Score {
    private long id;

    private Date date;
    private String name;
    private Level level;
    private long totalSeconds;

    public Score(Date date, String name, Level level, long totalSeconds) {
        this();

        this.date = date;
        this.name = name;
        this.level = level;
        this.totalSeconds = totalSeconds;
    }

    public Score() {
        this.id = -1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public long getTotalSeconds() {
        return totalSeconds;
    }

    public void setTotalSeconds(long totalSeconds) {
        this.totalSeconds = totalSeconds;
    }
}
