package ru.cft.focus.start.koreneva.task3.model;

import ru.cft.focus.start.koreneva.task3.helpers.TimeHelper;
import ru.cft.focus.start.koreneva.task3.model.interfaces.ITimeListener;
import ru.cft.focus.start.koreneva.task3.model.interfaces.ITimeService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TimeService implements ITimeService {
    private final List <ITimeListener> timeListenerList;
    private long startTimeMillis;
    private long endTimeMillis;
    private boolean started;

    public TimeService() {
        timeListenerList = new ArrayList <>();
    }

    public void addEventListener(ITimeListener event) {
        timeListenerList.add(event);
    }

    public void start() {
        if (started) {
            throw new IllegalStateException("You can't start started timer.");
        }

        startTimeMillis = System.currentTimeMillis();
        started = true;
    }

    public void stop() {
        if (!started) {
            throw new IllegalStateException("You can't stop stopped timer.");
        }

        endTimeMillis = System.currentTimeMillis();
        started = false;
    }

    public long getTotalSeconds() {
        return TimeUnit.MILLISECONDS.toSeconds(getTotalMillis());
    }

    @Override
    public boolean isStarted() {
        return started;
    }

    @Override
    public void updateTime() {
        for (ITimeListener listener : timeListenerList) {
            listener.setTime(TimeHelper.fromTotalSecondsAsString(getTotalSeconds()));
        }
    }

    private long getTotalMillis() {
        if (started) {
            return System.currentTimeMillis() - startTimeMillis;
        }

        return endTimeMillis - startTimeMillis;
    }
}
