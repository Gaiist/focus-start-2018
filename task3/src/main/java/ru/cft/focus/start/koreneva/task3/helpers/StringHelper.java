package ru.cft.focus.start.koreneva.task3.helpers;

public class StringHelper {
    private static final String EMPTY = "";

    private StringHelper() {
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.equals(EMPTY);
    }
}
