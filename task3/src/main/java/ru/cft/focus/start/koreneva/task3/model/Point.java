package ru.cft.focus.start.koreneva.task3.model;

abstract class Point {
    private final int x;
    private final int y;
    private boolean isClosed = true;
    private boolean isMarkedAsBomb = false;
    private PointType type;


    Point(int x, int y, PointType type) {
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException("X and Y can't be less than 0.");
        }

        this.x = x;
        this.y = y;
        this.type = type;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    void open() {
        isClosed = false;
    }

    boolean isClosed() {
        return isClosed;
    }

    void changeMarkedAsBomb() {
        isMarkedAsBomb = !isMarkedAsBomb;
    }

    BombPoint asBomb() {
        if (!( this instanceof BombPoint )) {
            throw new ClassCastException("Point not instanceof BombPoint");
        }

        return (BombPoint) this;
    }

    EmptyPoint asEmpty() {
        if (!( this instanceof EmptyPoint )) {
            throw new ClassCastException("Point not instanceof EmptyPoint");
        }

        return (EmptyPoint) this;
    }

    boolean isBomb() {
        return type == PointType.BOMB;
    }

    boolean isEmpty() {
        return type == PointType.EMPTY;
    }

    boolean isFlagged() {
        return isMarkedAsBomb;
    }
}
