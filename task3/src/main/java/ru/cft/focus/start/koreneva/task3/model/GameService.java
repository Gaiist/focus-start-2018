package ru.cft.focus.start.koreneva.task3.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task3.Level;
import ru.cft.focus.start.koreneva.task3.helpers.MatrixHelper;
import ru.cft.focus.start.koreneva.task3.helpers.StringHelper;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IGameCommands;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IGameEvents;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IHighScoreService;
import ru.cft.focus.start.koreneva.task3.model.interfaces.ITimeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class GameService implements IGameCommands {
    private static final Level DEFAULT_LEVEL = Level.BEGINNER;
    private final Logger logger;

    private final List <IGameEvents> gameEventsList;
    private final IHighScoreService highScoreService;
    private final ITimeService timeService;
    private final LevelInfoService levelInfoService;
    private final IMatrixGenerator matrixGenerator;

    private Point[][] pointMatrix;
    private Integer customBombCount;
    private int bombCounter;
    private boolean firstGame;
    private LevelInfoService.LevelInfo currentLevelInfo;

    public GameService(ITimeService timeService, IHighScoreService highScoreService, IMatrixGenerator matrixGenerator) {
        logger = LoggerFactory.getLogger(GameService.class);
        gameEventsList = new ArrayList <>();
        this.timeService = timeService;
        this.highScoreService = highScoreService;
        this.matrixGenerator = matrixGenerator;
        levelInfoService = new LevelInfoService();

        currentLevelInfo = levelInfoService.get(DEFAULT_LEVEL);
    }

    public void addEventListener(IGameEvents event) {
        gameEventsList.add(event);
    }

    @Override
    public void tryStartNewGame() {
        firstGame = true;
        if (timeService.isStarted()) {
            timeService.stop();
        }

        bombCounter = getBombCount();
        sendGameEvents();
        gameEventsList.forEach(events -> events.setMinesCount(bombCounter));
    }

    private int getBombCount() {
        return customBombCount == null ? currentLevelInfo.getBombCount() : customBombCount;
    }

    @Override
    public void tryOpen(int x, int y) {
        ensureCorrectCoordinates(x, y);

        generateMatrixIfNeeded(x, y);

        Point currentPoint = pointMatrix[x][y];

        if (!currentPoint.isClosed() || currentPoint.isFlagged()) {
            return;
        }

        if (currentPoint.isEmpty()) {
            openEmptyPoint(currentPoint.asEmpty());
        } else if (currentPoint.isBomb()) {
            openBombPoint(currentPoint.asBomb());
        }
    }

    @Override
    public void tryMarkAsBomb(int x, int y) {
        ensureCorrectCoordinates(x, y);
        generateMatrixIfNeeded(x, y);

        Point currentPoint = pointMatrix[x][y];
        if (!currentPoint.isClosed()) {
            return;
        }

        currentPoint.changeMarkedAsBomb();

        bombCounter += currentPoint.isFlagged() ? -1 : 1;
        for (IGameEvents events : gameEventsList) {
            if (currentPoint.isFlagged()) {
                events.setFlagged(x, y);
            } else {
                events.setClosed(x, y);
            }
            events.setMinesCount(bombCounter);
        }

        sendWinIfNeeded();
    }

    @Override
    public void tryChangeLevel(Level level) {
        currentLevelInfo = levelInfoService.get(level);
        customBombCount = null;
        this.tryStartNewGame();
    }

    @Override
    public void tryChangeBombCount(int bombCount) {
        if (bombCount < 0 || bombCount > currentLevelInfo.getSizeX() * currentLevelInfo.getSizeY()) {
            throw new IllegalArgumentException("Bomb count can't be less than 0 or matrix size.");
        }
        this.customBombCount = bombCount;
        tryStartNewGame();
    }

    @Override
    public void tryOpenHighScore() {
        String scores = highScoreService.getHighScoreInfoAsString();
        gameEventsList.forEach(events -> events.openHighScores(scores));
    }

    @Override
    public void trySetName(String name) {
        if (StringHelper.isNullOrEmpty(name)) {
            throw new IllegalArgumentException("Name can't be null or empty.");
        }

        Score score = new Score();
        score.setTotalSeconds(timeService.getTotalSeconds());
        score.setLevel(currentLevelInfo.getLevel());
        score.setName(name);
        score.setDate(new Date());
        highScoreService.add(score);
    }

    @Override
    public void tryUpdateTime() {
        timeService.updateTime();
    }

    @Override
    public void tryOpenEmptyPointMiddleClicked(int x, int y) {
        ensureCorrectCoordinates(x, y);
        generateMatrixIfNeeded(x, y);

        Point currentPoint = pointMatrix[x][y];

        if (currentPoint.isClosed() || !currentPoint.isEmpty()) {
            return;
        }

        List <Point> aroundPoints = MatrixHelper.getAroundItems(pointMatrix, x, y);
        long flaggedCount = aroundPoints.stream()
                .filter(Point::isFlagged)
                .count();

        if (flaggedCount != currentPoint.asEmpty().getAroundBombCount()) {
            return;
        }

        List <Point> notFlaggedPointList = aroundPoints.stream()
                .filter(p -> !p.isFlagged())
                .collect(Collectors.toList());

        for (Point pointIterator : notFlaggedPointList) {
            if (pointIterator.isBomb()) {
                openBombPoint(pointIterator.asBomb());
            } else {
                openEmptyPoint(pointIterator.asEmpty());
            }
            pointIterator.open();
        }
    }

    private void ensureCorrectCoordinates(int x, int y) {
        if (x < 0 || y < 0 ||
                x >= currentLevelInfo.getSizeX() || x >= currentLevelInfo.getSizeY()) {
            throw new IllegalArgumentException("Coordinates can't be less then 0 and " +
                    "more than size x and size y.");
        }
    }

    private void generateMatrixIfNeeded(int x, int y) {
        if (firstGame) {
            pointMatrix = generatePointMatrix(x, y);
            firstGame = false;
        }

        if (!timeService.isStarted()) {
            timeService.start();
        }
    }

    private void openEmptyPoint(EmptyPoint currentPoint) {
        List <EmptyPoint> updatedPoints = new ArrayList <>();
        openZeroPoints(currentPoint, updatedPoints);

        for (IGameEvents events : gameEventsList) {
            for (EmptyPoint updatedPoint : updatedPoints) {
                events.setEmpty(updatedPoint.getX(), updatedPoint.getY(), updatedPoint.getAroundBombCount());
            }
        }

        sendWinIfNeeded();
    }

    private void sendWinIfNeeded() {
        if (!checkGameOverAndWin()) {
            return;
        }

        timeService.stop();
        gameEventsList.forEach(IGameEvents::win);
        firstGame = true;
    }

    private void openZeroPoints(Point point, List <EmptyPoint> updatedPoints) {
        if (!point.isClosed() || !point.isEmpty() || point.isFlagged()) {
            return;
        }

        point.open();
        updatedPoints.add(point.asEmpty());
        if (point.asEmpty().getAroundBombCount() != 0) {
            return;
        }

        MatrixHelper.getAroundItems(pointMatrix, point.getX(), point.getY())
                .forEach(p -> openZeroPoints(p, updatedPoints));
    }

    private void openBombPoint(BombPoint currentPoint) {
        currentPoint.open();
        firstGame = false;
        gameEventsList.forEach(events -> events.setBomb(currentPoint.getX(), currentPoint.getY()));
        MatrixHelper.toList(pointMatrix)
                .stream()
                .filter(Point::isEmpty)
                .filter(Point::isFlagged)
                .forEach(point -> {
                    point.open();
                    gameEventsList.forEach(events -> events.setNoBomb(point.getX(), point.getY()));
                });

        timeService.stop();
        gameEventsList.forEach(IGameEvents::loose);
    }

    private boolean checkGameOverAndWin() {
        for (Point point : MatrixHelper.toList(pointMatrix)) {
            boolean notFlaggedBomb = point.isBomb() && !point.isFlagged();
            boolean closedEmpty = point.isEmpty() && point.isClosed();

            if (notFlaggedBomb || closedEmpty) {
                return false;
            }
        }

        return true;
    }

    private Point[][] generatePointMatrix(int noBombX, int noBombY) {
        int sizeX = currentLevelInfo.getSizeX();
        int sizeY = currentLevelInfo.getSizeY();

        boolean[][] boolMatrix = matrixGenerator.generateBooleanMatrix(
                noBombX,
                noBombY,
                sizeX,
                sizeY,
                getBombCount());

        Point[][] pointTypeMatrix = new Point[sizeX][sizeY];

        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                if (boolMatrix[x][y]) {
                    pointTypeMatrix[x][y] = new BombPoint(x, y);
                } else {
                    int aroundBombCount = calculateAroundBombCount(boolMatrix, x, y);
                    pointTypeMatrix[x][y] = new EmptyPoint(x, y, aroundBombCount);
                }
            }
        }

        return pointTypeMatrix;
    }

    private int calculateAroundBombCount(boolean[][] boolMatrix, int x, int y) {
        Boolean[][] objectMatrix = MatrixHelper.convertToObjectBoolean(boolMatrix);
        List <Boolean> points = MatrixHelper.getAroundItems(objectMatrix, x, y);
        int aroundBombCount = 0;

        for (Boolean isBomb : points) {
            if (isBomb) {
                aroundBombCount++;
            }
        }

        return aroundBombCount;
    }

    private void sendGameEvents() {
        logger.info("send GameEvents");
        int sizeX = currentLevelInfo.getSizeX();
        int sizeY = currentLevelInfo.getSizeY();

        gameEventsList.forEach(e -> e.setClosedAll(sizeX, sizeY));
    }
}
