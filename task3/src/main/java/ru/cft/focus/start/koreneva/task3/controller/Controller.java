package ru.cft.focus.start.koreneva.task3.controller;

import ru.cft.focus.start.koreneva.task3.Level;
import ru.cft.focus.start.koreneva.task3.model.interfaces.IGameCommands;
import ru.cft.focus.start.koreneva.task3.view.interfaces.IViewEvents;

public class Controller implements IViewEvents {
    private final IGameCommands gameCommands;

    public Controller(IGameCommands gameCommands) {
        if (gameCommands == null) {
            throw new IllegalArgumentException("Game commands can't be null.");
        }

        this.gameCommands = gameCommands;
    }

    @Override
    public void tryStartNewGame() {
        gameCommands.tryStartNewGame();
    }

    @Override
    public void tryOpenEmptyPointMiddleClicked(int x, int y) {
        gameCommands.tryOpenEmptyPointMiddleClicked(x, y);
    }

    @Override
    public void tryOpen(int x, int y) {
        gameCommands.tryOpen(x, y);
    }

    @Override
    public void tryMarkAsBomb(int x, int y) {
        gameCommands.tryMarkAsBomb(x, y);
    }

    @Override
    public void tryChangeLevel(Level level) {
        gameCommands.tryChangeLevel(level);
    }

    @Override
    public void tryChangeBombCount(int bombCount) {
        gameCommands.tryChangeBombCount(bombCount);
    }

    @Override
    public void tryOpenHighScore() {
        gameCommands.tryOpenHighScore();
    }

    @Override
    public void trySetName(String name) {
        gameCommands.trySetName(name);
    }

    @Override
    public void tryUpdateTime() {
        gameCommands.tryUpdateTime();
    }
}
