package ru.cft.focus.start.koreneva.task3.model.interfaces;

import ru.cft.focus.start.koreneva.task3.model.Score;

import java.util.List;

public interface IHighScoreRepository {
    List <Score> getAll();

    void add(Score score);

    void delete(long id);
}
