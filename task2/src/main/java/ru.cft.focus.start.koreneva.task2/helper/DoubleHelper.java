package ru.cft.focus.start.koreneva.task2.helper;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DoubleHelper {
    public double round(double number, int digits) {
        return BigDecimal.valueOf(number).setScale(digits, RoundingMode.HALF_UP).doubleValue();
    }
}
