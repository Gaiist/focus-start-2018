package ru.cft.focus.start.koreneva.task2;

import ru.cft.focus.start.koreneva.task2.exceptions.FigureTypeParseException;
import ru.cft.focus.start.koreneva.task2.exceptions.TriangleDoesNotExistException;
import ru.cft.focus.start.koreneva.task2.figure.Figure;

import java.util.List;

public class Solution {
    private static final String FILE_PATH = "task2/src/main/java/ru.cft.focus.start.koreneva.task2/files/rectangle";

    public static void main(String[] args) throws FigureTypeParseException, TriangleDoesNotExistException {
        ReadFileService fileReadService = new ReadFileService();
        ParseFigureService figureFactory = new ParseFigureService();

        List <String> list = fileReadService.readAllLines(FILE_PATH);
        Figure figure = figureFactory.parseFigure(list.get(0), list.get(1));

        System.out.println(figure);
    }
}
