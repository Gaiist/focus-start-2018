package ru.cft.focus.start.koreneva.task2;

import ru.cft.focus.start.koreneva.task2.exceptions.TriangleDoesNotExistException;
import ru.cft.focus.start.koreneva.task2.figure.Figure;

@FunctionalInterface
interface IFigureParser {
    Figure parse(String parameters) throws TriangleDoesNotExistException;
}
