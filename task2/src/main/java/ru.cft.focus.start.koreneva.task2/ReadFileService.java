package ru.cft.focus.start.koreneva.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task2.exceptions.EmptyFileException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;

class ReadFileService {
    private final Logger logger = LoggerFactory.getLogger(ReadFileService.class);
    private List <String> lines;

    List <String> readAllLines(String filePath) {
        try {
            lines = Files.readAllLines(Paths.get(filePath));

            if (lines.isEmpty()) {
                throw new EmptyFileException();
            }
        } catch (NoSuchFileException e) {
            logger.error("File not found.");
        } catch (EmptyFileException e) {
            logger.error("File is empty. Please, check it.");
        } catch (IOException e) {
            logger.error("Fatal error.", e);
        }
        return lines;
    }
}
