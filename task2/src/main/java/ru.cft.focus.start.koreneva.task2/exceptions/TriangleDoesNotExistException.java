package ru.cft.focus.start.koreneva.task2.exceptions;

public class TriangleDoesNotExistException extends Exception {
    public TriangleDoesNotExistException() {
        super("Triangle does not exist. Please, check the settings in the file.");
    }
}
