package ru.cft.focus.start.koreneva.task2.exceptions;

public class FigureTypeParseException extends Exception {
    public FigureTypeParseException(String figureType) {
        super(String.format("Can't parse figure type: %s", figureType));
    }
}
