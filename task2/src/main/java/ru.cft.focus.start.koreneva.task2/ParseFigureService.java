package ru.cft.focus.start.koreneva.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task2.exceptions.FigureTypeParseException;
import ru.cft.focus.start.koreneva.task2.exceptions.TriangleDoesNotExistException;
import ru.cft.focus.start.koreneva.task2.figure.Circle;
import ru.cft.focus.start.koreneva.task2.figure.Figure;
import ru.cft.focus.start.koreneva.task2.figure.Rectangle;
import ru.cft.focus.start.koreneva.task2.figure.Triangle;

import java.util.Dictionary;
import java.util.Hashtable;

class ParseFigureService {
    private static final String MESSAGE = "Unidentified symbol. Please check your file.";
    private final Logger logger = LoggerFactory.getLogger(ParseFigureService.class);
    private Dictionary <FigureNames, IFigureParser> figureParsers;

    ParseFigureService() throws TriangleDoesNotExistException {
        figureParsers = new Hashtable <>();
        initializeFigures();
    }

    Figure parseFigure(String name, String parameters) throws FigureTypeParseException, TriangleDoesNotExistException {
        FigureNames figureName = parseFigureType(name);

        return figureParsers.get(figureName).parse(parameters);
    }

    private FigureNames parseFigureType(String type) throws FigureTypeParseException {
        try {
            return FigureNames.valueOf(type);
        } catch (IllegalArgumentException e) {
            throw new FigureTypeParseException(type);
        }
    }

    private void initializeFigures() {
        figureParsers.put(FigureNames.CIRCLE, parameters -> {
            try {
                double radius = Double.parseDouble(parameters);
                return new Circle(radius);
            } catch (NumberFormatException e) {
                logger.error(MESSAGE, e);
                throw e;
            }
        });
        figureParsers.put(FigureNames.RECTANGLE, parameters -> {
            try {
                String[] heightAndLength = parameters.split(" ");
                return new Rectangle(Double.parseDouble(heightAndLength[0]), Double.parseDouble(heightAndLength[1]));
            } catch (NumberFormatException e) {
                logger.error(MESSAGE, e);
                throw e;
            }
        });
        figureParsers.put(FigureNames.TRIANGLE, parameters -> {
            try {
                String[] side1AndSide2AndSide3 = parameters.split(" ");
                return new Triangle(Double.parseDouble(side1AndSide2AndSide3[0]),
                        Double.parseDouble(side1AndSide2AndSide3[1]), Double.parseDouble(side1AndSide2AndSide3[2]));
            } catch (NumberFormatException e) {
                logger.error(MESSAGE, e);
                throw e;
            }
        });
    }
}

