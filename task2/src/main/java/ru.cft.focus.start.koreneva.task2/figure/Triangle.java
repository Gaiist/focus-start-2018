package ru.cft.focus.start.koreneva.task2.figure;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focus.start.koreneva.task2.exceptions.TriangleDoesNotExistException;
import ru.cft.focus.start.koreneva.task2.helper.DoubleHelper;


public class Triangle extends Figure {
    private static final String NAME = "Треугольник";
    private static final String LINE_SEPARATOR = "line.separator";
    private static final int DIGITS = 2;
    private static final Logger logger = LoggerFactory.getLogger(Triangle.class);

    private final double firstSide;
    private final double secondSide;
    private final double thirdSide;

    private final DoubleHelper helper = new DoubleHelper();

    public Triangle(double firstSide, double secondSide, double thirdSide) throws TriangleDoesNotExistException {
        super(NAME);

        this.firstSide = firstSide;
        this.secondSide = secondSide;
        this.thirdSide = thirdSide;

        if (firstSide + secondSide <= thirdSide || firstSide + thirdSide <= secondSide || secondSide + thirdSide <= firstSide) {
            logger.error("Triangle does not exist.");
            throw new TriangleDoesNotExistException();
        }
    }

    @Override
    public String toString() {
        String information = "";

        information = information.concat(super.toString());

        information = information.concat("Длина первой стороны = " + firstSide);
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Длина второй стороны = " + secondSide);
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Длина третьей стороны = " + thirdSide);
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Противолежаший угол для первой стороны = "
                + calculateOppositeAngleToFirstSide());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Противолежаший угол для второй стороны = "
                + calculateOppositeAngleToSecondSide());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Противолежаший угол для третьей стороны = "
                + calculateOppositeAngleToThirdSide());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        return information;
    }

    public double calculateSquare() {
        double p = calculatePerimeter() / 2;
        double square = Math.sqrt(p * ( p - firstSide ) * ( p - secondSide ) * ( p - thirdSide ));

        return helper.round(square, DIGITS);
    }

    double calculatePerimeter() {
        double perimeter = firstSide + secondSide + thirdSide;

        return helper.round(perimeter, DIGITS);
    }

    private double calculateOppositeAngleToFirstSide() {
        return calculateOppositeAngle(firstSide, secondSide, thirdSide);
    }

    private double calculateOppositeAngleToSecondSide() {
        return calculateOppositeAngle(secondSide, firstSide, thirdSide);
    }

    private double calculateOppositeAngleToThirdSide() {
        return calculateOppositeAngle(thirdSide, secondSide, firstSide);
    }

    private double calculateOppositeAngle(double oppositeSide, double side1, double side2) {
        double cos = ( ( squaring(side1) + squaring(side2) ) - squaring(oppositeSide) )
                / ( 2 * side1 * side2 );
        double degrees = conversionCosineInDegrees(cos);

        return helper.round(degrees, DIGITS);
    }

    private double conversionCosineInDegrees(double cos) {
        return ( 180 / Math.PI ) * Math.acos(cos);
    }

    private double squaring(double number) {
        return number * number;
    }
}
