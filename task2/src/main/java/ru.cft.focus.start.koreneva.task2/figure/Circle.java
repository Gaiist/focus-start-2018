package ru.cft.focus.start.koreneva.task2.figure;


import ru.cft.focus.start.koreneva.task2.helper.DoubleHelper;

public class Circle extends Figure {
    private static final String NAME = "Круг";
    private static final String LINE_SEPARATOR = "line.separator";
    private static final int DIGITS = 2;
    private final double radius;
    private final DoubleHelper helper = new DoubleHelper();

    public Circle(double radius) {
        super(NAME);

        this.radius = radius;
    }

    @Override
    public String toString() {
        String information = "";

        information = information.concat(super.toString());

        information = information.concat("Радиус = " + radius);
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Диаметр = " + calculateDiameter());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        return information;
    }

    public double calculateSquare() {
        double square = Math.round(Math.PI * radius * radius);

        return helper.round(square, DIGITS);
    }

    double calculatePerimeter() {
        double perimeter = Math.PI * 2 * radius;

        return helper.round(perimeter, DIGITS);
    }

    private double calculateDiameter() {
        return helper.round(radius * 2, DIGITS);
    }
}
