package ru.cft.focus.start.koreneva.task2.figure;


import ru.cft.focus.start.koreneva.task2.helper.DoubleHelper;

public class Rectangle extends Figure {
    private static final String NAME = "Прямоугольник";
    private static final String LINE_SEPARATOR = "line.separator";
    private static final int DIGITS = 2;

    private final double height;
    private final double length;

    private final DoubleHelper helper = new DoubleHelper();


    public Rectangle(double height, double length) {
        super(NAME);

        this.height = height;
        this.length = length;
    }

    @Override
    public String toString() {
        String information = "";

        information = information.concat(super.toString());

        information = information.concat("Длина = " + length);
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Ширина = " + height);
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Длина диагонали = " + calculateDiagonal());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        return information;
    }

    public double calculateSquare() {
        return helper.round(height * length, DIGITS);
    }

    double calculatePerimeter() {
        double perimeter = ( height + length ) * 2;

        return helper.round(perimeter, DIGITS);
    }

    private double calculateDiagonal() {
        double diagonal = Math.sqrt(height * height + length * length);

        return helper.round(diagonal, DIGITS);
    }
}
