package ru.cft.focus.start.koreneva.task2.figure;

public abstract class Figure {
    private static final String LINE_SEPARATOR = "line.separator";
    private final String name;

    Figure(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be null or empty.");
        }
        this.name = name;
    }

    private String getName() {
        return name;
    }

    public abstract double calculateSquare();

    abstract double calculatePerimeter();

    public String toString() {
        String information = "";

        information = information.concat("Название: " + getName());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Площадь = " + calculateSquare());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        information = information.concat("Периметр = " + calculatePerimeter());
        information = information.concat(System.getProperty(LINE_SEPARATOR));

        return information;
    }
}